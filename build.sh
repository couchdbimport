#!/bin/sh
GCC=`which gcc`
export GCC

CPP=`which g++`
export CPP

MAKE=`which make`
export MAKE

UNAME=`which uname`
export UNAME

HOST=`$UNAME`
export HOST

BUILD_DIR=build
export BUILD_DIR

COUCHDB_VERSION=`cat ./src/version`
export COUCHDB_VERSION

COUCH_DIR=build/couch-$COUCHDB_VERSION/ebin
COUCHDB_INSTALL_DIR=/usr/local/


#MAKEFILE=" -f build/Makefile"


ERLANG_BIN_DIR=false
INSTALL_COUCHDB=false
# taken from autoconf
while test $# != 0; do
	case $1 in
		--*=*)
			ac_option=`expr "x$1" : 'x\([^=]*\)='`
			ac_optarg=`expr "x$1" : 'x[^=]*=\(.*\)'`
			ac_shift=:
		;;
		-*)
			ac_option=$1
			ac_optarg=$2
			ac_shift=shift
		;;
		*) # This is not an option, so the user has probably given explicit
		   # arguments.
			 ac_option=$1
			 ac_need_defaults=false;;
	esac

	case $ac_option in
		--install)
			INSTALL_COUCHDB=true
			COUCHDB_INSTALL_DIR=$ac_optarg
			if [ -z "$COUCHDB_INSTALL_DIR" ]; then
				COUCHDB_INSTALL_DIR=/usr/local
				export COUCHDB_INSTALL_DIR
			fi
		;;
		
		--with-erlang)
			ERLANG_BIN_DIR=$ac_optarg
			export ERLANG_BIN_DIR
		;;	
		
		--help)
			echo "Available options: --install which installs CouchDb and --with-erlang"
			exit 0;
		;;
		
		-*)
			echo "Unknown option $ac_option. Try --help"
			exit 1;
	esac
	shift
done


if [ "$ERLANG_BIN_DIR" != "false" ]; then
	ERLC=$ERLANG_BIN_DIR/erlc
	export ERLC
	ERL=$ERLANG_BIN_DIR/erl
	export ERL
fi

if [ ! -x "$ERLC" ]; then
	export ERLC=`which erlc`
fi

if [ ! -x "$ERLC" ]; then
	echo "Cannot find erlc. Please set ERLANG_BIN_DIR manually. (e.g. export ERLANG_BIN_DIR=/usr/local/erlang/bin)"
	exit 1
fi

if [ ! -x "$ERL" ]; then
	export ERL=`which erl`
fi

if [ ! -x "$ERL" ]; then
	echo "Cannot find erl. Please set ERLANG_BIN_DIR manually. (e.g. export ERLANG_BIN_DIR=/usr/local/erlang/bin)"
	exit 1
fi

if [ -z "$ERLANG_INCLUDE_DIR" ]; then
	ERLANG_INCLUDE_DIR=/usr/include
	export ERLANG_INCLUDE_DIR
fi

if [ ! -e "$ERLANG_INCLUDE_DIR/erl_driver.h" ]; then
	ERLANG_INCLUDE_DIR=/usr/lib/erlang/usr/include/
	export ERLANG_INCLUDE_DIR

	echo "Cannot find erl_driver.h. Please set ERLANG_INCLUDE_DIR manually and make sure it points to the directory that contains erl_driver.h. (e.g. export ERLANG_INCLUDE_DIR=/usr/local/erlang/usr/include/)"
	exit 1
fi

if [ ! -d  "$COUCH_DIR" ]; then
	mkdir -p $COUCH_DIR
fi

COUCH_BIG_ENDIAN=`uname -a|grep powerpc`;

if [ ! -z "$COUCH_BIG_ENDIAN" ]; then
	COUCH_BIG_ENDIAN=true
	export COUCH_BIG_ENDIAN
fi



if [ "$INSTALL_COUCHDB" = "true" ]; then

	echo "Installing CouchDb into $COUCHDB_INSTALL_DIR"

	if [ -d  "$COUCHDB_INSTALL_DIR/couchdb" ]; then
		rm -rf "$COUCHDB_INSTALL_DIR/old.couchdb"
		mv "$COUCHDB_INSTALL_DIR/couchdb" "$COUCHDB_INSTALL_DIR/old.couchdb"
	fi

	mkdir -p $COUCHDB_INSTALL_DIR/couchdb
	
	
	COUCHDB_INSTALL_DIR=$COUCHDB_INSTALL_DIR/couchdb
	export COUCHDB_INSTALL_DIR

	$MAKE $MAKEFILE install
	exit 0
fi

$MAKE $J $MAKEFILE couchdb