load("json.js");

var cmd;
var functions = [];
while (cmd = eval(readline())) {
  switch (cmd[0]) {
    case "reset":
      functions = [];
      gc();
      print("true");
      break;
    case "add_map_fun":
      // second arg is a string that will compile to a function
      try {
        functionObject = eval(cmd[1]);
      } catch (e) {
        print((e.toString() + " (" + cmd[1].toJSONString() + ")").toJSONString());
        break;
      }
      if (typeof functionObject == "function") {
        functions.push(functionObject);
        print("true");
      }
      else {
        print(("TypeError: Expression does not eval to a function. (" + cmd[1].toJSONString() + ")").toJSONString());
      }
      break;
    case "map_doc":
      var results = [];
      var doc = cmd[1];
      seal(doc); // seal to prevent map functions from changing doc
      for(i=0; i < functions.length; i++) {
        try {
          var result = functions[i](doc);
          if (result === undefined)
            results[i] = 0; // indicate no match
          else if (result === 0)
            results[i] = {value:0};
          else
            results[i] = result;
        } catch (err) {
          // An error mapping the document. Indicate no match.
          results[i] = 0; // indicate no match
        }
      }
      print(results.toJSONString());
      break;
    default:
      print("error");
      exit(1);
  }
}