%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

%
% This causes an OS process to spawned and it is notified every time a database is updated.
%
% The notifications are in the form of a the database name sent as a line of text to the OS processes
% stdout.
%

-module(couch_db_update_notifier).

-behaviour(gen_event).

-export([start_link/1, notify_all/1]).
-export([init/1, terminate/2, handle_event/2, handle_call/2, handle_info/2, code_change/3,stop/1]).

-define(ERR_HANDLE, {Port, {exit_status, Status}} -> {stop, {unknown_error, Status}, {unknown_error, Status}, Port}).

start_link(Exec) ->
	couch_event_sup:start_link(couch_db_update, {couch_db_update_notifier, make_ref()}, Exec).
	
notify_all(DbName) ->
	gen_event:notify(couch_db_update, {db_updated, DbName}).
	
stop(Pid) ->
	couch_event_sup:stop(Pid).

init(Exec) ->
	Port = open_port({spawn, Exec}, [stream, exit_status, hide]),
	{ok, Port}.

terminate(_Reason, _Port) ->
	ok.

handle_event({db_updated, DbName}, Port) ->
	%% send a notification
	true = port_command(Port, DbName ++ "\n"),
	{ok, Port}.

handle_call(_Request, State) ->
	{ok, ok, State}.
	
handle_info({'EXIT', _, Reason}, Port) ->
	remove_handler.
	
code_change(_OldVsn, State, _Extra) ->
	{ok, State}.
