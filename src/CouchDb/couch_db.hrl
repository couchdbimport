%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


-define(NON_REP_DOC_PREFIX, "_local_").
-define(DESIGN_DOC_PREFIX, "_design_").

-record(doc_info,
	{
	uuid = [],
	revision = [],
	update_seq = 0,
	summary_pointer = nil,
	conflict_revs = [],
	deleted_conflict_revs = [],
	deleted = false	
	}).

-record(doc,
	{
	uuid = "",
	revisions = [],
	
	% the json body object. 
	body = "",
	
	% each attachment contains:
	%    {data, Type, <<binary>>}
	% or:
	%    {pointer, Type, {FileHandle, StreamPointer, Length}}
	attachments = [],
	
	deleted = false
	}).