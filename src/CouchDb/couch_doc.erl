%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-module(couch_doc).

-export([get_view_functions/1, is_special_doc/1]).
-export([bin_foldl/3,bin_size/1,bin_to_binary/1]).
-export([from_json_obj/1,to_json_obj/2]).

-include("couch_db.hrl").

to_json_obj(Doc, DoFullDoc) ->
	{obj, [{"_id", Doc#doc.uuid}] ++ 
		case Doc#doc.revisions of
			[] -> [];
			_ -> [{"_rev", lists:nth(1, Doc#doc.revisions)}]
		end ++
		case Doc#doc.deleted of
			true -> [{"_deleted", true}];
			_  -> []
		end ++
		case Doc#doc.body of
			{obj, BodyProps} -> BodyProps;
			_ -> [{"body", Doc#doc.body}]
		end ++
		case DoFullDoc of
		true -> % return the full revision list and the binaries as strings.
			BinProps = lists:map(
				fun({Name, {"base64", BinValue}}) -> 
					{Name, {obj, [{"type", "base64"},
									{"data", couch_util:encodeBase64(bin_to_binary(BinValue))}]}};
				({Name, {Type, BinValue}}) ->
					{Name, {obj, [{"type", Type},
									{"data", binary_to_term(bin_to_binary(BinValue))}]}}
				end,
				Doc#doc.attachments),
			[{"_revisions", list_to_tuple(Doc#doc.revisions)}] ++	
			case BinProps of
				[] -> [];
				_ -> [{"_attachments", {obj, BinProps}}]
			end;
		false ->
			BinProps = lists:map(
				fun({Name, {Type, BinValue}}) -> 
					{Name, {obj, [{"stub", true}, {"type", Type},
									{"length", bin_size(BinValue)}]}}
				end,
				Doc#doc.attachments),
			case BinProps of
				[] -> [];
				_ -> [{"_attachments", {obj, BinProps}}]
			end
		end
		}.

from_json_obj({obj, Props}) ->
	{obj,JsonBins} = proplists:get_value("_attachments", Props, {obj, []}),
	Bins = lists:map(fun({Name, {obj, BinProps}}) ->
		Value = proplists:get_value("data", BinProps),
		case proplists:get_value("type", BinProps) of
		"base64" ->
			{Name, {"base64", couch_util:decodeBase64(Value)}};
		Type ->
			{Name, {Type, term_to_binary(Value)}}
		end
	end, JsonBins),
	#doc{
		uuid = proplists:get_value("_id", Props, ""),
		revisions = tuple_to_list(proplists:get_value("_revisions", Props, {})),
		body = {obj, [{Key, Value} || {[FirstChar|_]=Key, Value} <- Props, FirstChar /= $_]},
		attachments = Bins
		}.
	
is_special_doc(?DESIGN_DOC_PREFIX ++ _ ) ->
	true;
is_special_doc(#doc{uuid=Id}) ->
	is_special_doc(Id);
is_special_doc(_) ->
	false.
	
bin_foldl(Bin, Fun, Acc) when is_binary(Bin) ->	
	case Fun(Bin, Acc) of
		{ok, Acc2} -> {ok, Acc2};
		{done, Acc2} -> {ok, Acc2}
	end;
bin_foldl({Fd, Sp, Len}, Fun, Acc) ->	
	{ok, Acc2, _Sp2} = couch_stream:foldl(Fd, Sp, Len, Fun, Acc),
	{ok, Acc2}.	
	
bin_size(Bin) when is_binary(Bin) ->	
	size(Bin);
bin_size({_Fd, _Sp, Len}) ->
	Len.
	
bin_to_binary(Bin) when is_binary(Bin) ->	
	Bin;
bin_to_binary({Fd, Sp, Len}) ->
	{ok, Bin, _Sp2} = couch_stream:read(Fd, Sp, Len),
	Bin.

get_view_functions(#doc{body={obj, Fields}}) ->
	Lang = proplists:get_value("language", Fields, "text/javascript"),
	{obj, Views} = proplists:get_value("views", Fields, {obj, []}),
	{Lang, [{ViewName, Value} || {ViewName, Value} <- Views, is_list(Value)]};
get_view_functions(_Doc) ->
	none.
