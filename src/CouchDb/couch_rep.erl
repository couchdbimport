%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-module(couch_rep).

-include("couch_db.hrl").

-export([replicate/2, replicate/3, test/0, test_write_docs/3]).

-record(rep_rec,
	{seq_a = 0,
	seq_b = 0,
	session_id = 0
	}).

	

replicate(DbNameA, DbNameB) ->
	replicate(DbNameA, DbNameB, []).

replicate(DbNameA, DbNameB, Options) ->
	{ok, DbA} =
	case DbNameA of
		"http://" ++ _ -> 	{ok, DbNameA};
		_ ->				couch_server:open(DbNameA)
	end,
	
	{ok, DbB} =
	case DbNameB of
		"http://" ++ _ ->	{ok, DbNameB};
		_ ->				couch_server:open(DbNameB)
	end,
	
	{ok, HostName} = inet:gethostname(),	
	
	RepRecKeyString =
	HostName ++ "|" ++ 
	case DbNameA > DbNameB of
		% order the names consistently, in case they 
		% are entered in a different order in the args
		true ->		DbNameA ++ "|" ++ DbNameB;
		false ->	DbNameB ++ "|" ++ DbNameA
	end,
	
	<<RepRecHashedKeyInt:128/integer>> = erlang:md5(RepRecKeyString),	
	RepRecHashedKey = integer_to_list(RepRecHashedKeyInt),
	
	{SeqNumA, SeqNumB} = 
	case lists:member(full, Options) of
	true ->
		{0, 0};
	false ->
		RepRecA = get_rep_rec(DbA, RepRecHashedKey),
		RepRecB = get_rep_rec(DbB, RepRecHashedKey),
	
		case RepRecA == RepRecB of
		true ->
			% if the records are identical, then we have a valid replication history
			{RepRecA#rep_rec.seq_a, RepRecA#rep_rec.seq_b};
		false ->
			io:format("Performing full replication."),
			{0, 0}
		end
	end,
	
	NewSeqNumA = pull_rep(DbB, DbA, SeqNumA),
	NewSeqNumB = pull_rep(DbA, DbB, SeqNumB),
	
	case NewSeqNumA == SeqNumA andalso NewSeqNumB == SeqNumB of
	true ->
		% nothing changed, don't record results
		ok;
	false ->
		% something changed, record results
		NewRec = #rep_rec{
				seq_a = NewSeqNumA,
				seq_b = NewSeqNumB,
				session_id = couch_util:rand32()},
	
		ok = set_rep_rec(DbA, RepRecHashedKey, NewRec),
		ok = set_rep_rec(DbB, RepRecHashedKey, NewRec)
	end.
	

	
pull_rep(DbTarget, DbSource, SourceSeqNum) ->
	{ok, NewSeq} = 
	enum_docs_since(DbSource, SourceSeqNum,
		fun(#doc_info{update_seq=Seq}=SrcDocInfo, _) ->
			maybe_save_docs(DbTarget, DbSource, SrcDocInfo),
			{ok, Seq}
		end, SourceSeqNum),
	NewSeq.
	
	
maybe_save_docs(DbTarget, DbSource, #doc_info{uuid=Uuid, revision=SrcRev, conflict_revs=SrcConflicts}) ->
	SrcRevs = [SrcRev | SrcConflicts],
	{ok, [{Uuid, MissingRevs}]} = get_missing_revs(DbTarget, [{Uuid, SrcRevs}]),
	
	case MissingRevs of
	[] ->
		ok;
	_Else ->
		
		% the 'ok' below validates no unrecoverable errors (like network failure, etc).
		{ok, DocResults} = open_doc_revs(DbSource, Uuid, MissingRevs, [latest]),
		
		% now we gloss over any errors opening the individual docs (security errors, etc).
		Docs = [RevDoc || {ok, RevDoc} <- DocResults], % only match successful loads
		
		
		case Docs of
		[] ->
			ok;
		_ ->
			% the 'ok' below validates no unrecoverable errors (like network failure, etc).
			{ok, _RevUpdatesResults} = save_doc_revs(DbTarget, Docs, []),
		
			% RevUpdatesResults contains information about if each revision
			% was successfully saved. Ignore for now, maybe forever. There may be
			% a security or validation model failing each save, but that's allowable
			% in succesful replication.
		
			ok
		end
	end.
	
	

get_rep_rec(Db, Key) ->
	case open_doc(Db, ?NON_REP_DOC_PREFIX ++ Key, []) of
	{ok, Doc} ->
		[SeqA] = couch_doc:get_field(Doc, "seq_a", [0]),
		[SeqB] = couch_doc:get_field(Doc, "seq_b", [0]),
		[SessionId] = couch_doc:get_field(Doc, "session_id", [0]),
		#rep_rec{seq_a=SeqA, seq_b=SeqB, session_id=SessionId};
	{not_found, missing} ->
		#rep_rec{}
	end.

set_rep_rec(Db, Key, #rep_rec{seq_a=SeqA, seq_b=SeqB, session_id=SessionId}) ->	
	Doc = #doc{uuid= ?NON_REP_DOC_PREFIX ++ Key},
	Doc2 = couch_doc:set_fields(Doc,
			[{"seq_a", [SeqA]},
			{"seq_b", [SeqB]},
			{"session_id", [SessionId]}]),
	{ok, 0} = save_doc(Db, Doc2, []),
	ok.
	
do_http_request(Url, Action, XmlBody) ->
	Request =
	case XmlBody of
	[] ->
		{Url, []};
	_ ->
		{Url, [], "application/json; charset=utf-8", XmlBody}
	end,
	{ok, {{_, ResponseCode,_},_Headers, ResponseBody}} = http:request(Action, Request, [], []),
	if
	ResponseCode >= 200, ResponseCode < 500 ->
		cjson:decode(ResponseBody)
	end.
	
	
enum_docs_since(DbUrl, StartSeq, InFun, InAcc) when is_list(DbUrl) ->
	Url = DbUrl ++ "_all_docs_by_update_seq?startkey=\"" ++ integer_to_list(StartSeq) ++ "\"",
	case do_http_request(Url, get, []) of
	{docs_by_update_seq, DocInfos} ->
		Result =
		(catch lists:foldl(fun(DocInfo, {ok, LocalAcc}) ->
				case InFun(DocInfo, LocalAcc) of
				{ok, LocalAcc2} ->
					{ok, LocalAcc2};
				{stop, LocalAcc2} ->
					throw({stop, LocalAcc2})
				end
			end,
			{ok, InAcc}, DocInfos)),
		case Result of
		{stop, InAcc2} ->
			{ok, InAcc2};
		{ok, InAcc2} ->
			{ok, InAcc2}
		end;
	{error, Error} ->
		throw(Error)
	end;
enum_docs_since(DbSource, StartSeq, Fun, Acc) ->
	couch_db:enum_docs_since(DbSource, StartSeq, Fun, Acc).
	
get_missing_revs(DbUrl, DocIdRevsList) when is_list(DbUrl) ->
	{obj, ResponseMembers} =
	do_http_request(DbUrl ++ "_missing_revs",
		post, cjson:encode(list_to_tuple(DocIdRevsList))),
	DocMissingRevsList = proplist:get_value("missing_revs", ResponseMembers),
	{ok, DocMissingRevsList};
get_missing_revs(Db, DocId) ->
	couch_db:get_missing_revs(Db, DocId).

	
save_doc(DbUrl, #doc{uuid=DocId}=Doc, _Options) when is_list(DbUrl) ->	
	Url = DbUrl ++ DocId,
	{obj, ResponseMembers} =
		do_http_request(Url, put, couch_doc:to_json_obj(Doc, true)),
 	RevId = proplist:get_value("_rev", ResponseMembers),
	{ok, RevId};
save_doc(Db, Doc, Options) ->
	couch_db:save_doc(Db, Doc, Options).


save_doc_revs(DbUrl, [#doc{uuid=DocId}|_]=Docs, []) when is_list(DbUrl) ->
	_JsonDocs =
		[mod_couch:doc_to_xmerl(Doc, true) || Doc <- Docs],

	Url = DbUrl ++ DocId,
	case do_http_request(Url, put, blah) of
	{update_results, Results} ->
		{ok, Results};
	{error, Error} ->
		Error
	end;	
save_doc_revs(Db, Docs, Options) ->
	couch_db:save_doc_revs(Db, Docs, Options).


open_doc(DbUrl, DocId, []) when is_list(DbUrl) ->
	case do_http_request(DbUrl ++ DocId, get, []) of
	{doc, Doc} ->
		{ok, Doc};
	{error, Error} ->
		Error
	end;	
open_doc(Db, DocId, Options) when not is_list(Db) ->
	couch_db:open_doc(Db, DocId, Options).
	

open_doc_revs(DbUrl, DocId, Revs, Options) when is_list(DbUrl) ->
	QueryOptionStrs =
	lists:map(fun(latest) ->
			% latest is only option right now
			"latest=true"
		end, Options),
	ok,
	RevsQueryStrs = ["rev=" ++ couch_util:revid_to_list(Rev) || Rev <- Revs],
	Url = DbUrl ++ DocId ++ "?" ++ couch_util:implode(["full=true"] ++ QueryOptionStrs ++ RevsQueryStrs, "&"),
	case do_http_request(Url, get, []) of
	{doc_revs, Results} ->
		{ok, Results};
	{error, Error} ->
		Error
	end;	
open_doc_revs(Db, DocId, Revs, Options) ->
	couch_db:open_doc_revs(Db, DocId, Revs, Options).
		

		
		
		
test() ->
	couch_server:start(),
	%{ok, LocalA} = couch_server:open("replica_a"),
	{ok, LocalA} = couch_server:create("replica_a", [overwrite]),
	{ok, _} = couch_server:create("replica_b", [overwrite]),
	%DbA = "replica_a",
	DbA = "http://localhost:8888/replica_a/",
	%DbB = "replica_b",
	DbB = "http://localhost:8888/replica_b/",
	DocUnids = test_write_docs(10, LocalA, []),
	replicate(DbA, DbB),
	{ok, _Rev} = couch_db:delete_doc(LocalA, lists:nth(1, DocUnids), any),
	% replicate(DbA, DbB),
	ok.

test_write_docs(0, _Db, Output) ->
	lists:reverse(Output);
test_write_docs(N, Db, Output) ->
	Doc = couch_doc:new(),
	Doc2 = couch_doc:set_field(Doc, "foo", [integer_to_list(N)]),
	Doc3 = couch_doc:set_field(Doc2, "num", [N]),
	Doc4 = couch_doc:set_field(Doc3, "bar", ["blah"]),
	couch_db:save_doc(Db, Doc4, []),
	test_write_docs(N-1, Db, [Doc3#doc.uuid | Output]).
	
