%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-module(couch_server_sup).
-behaviour(supervisor).

-define(DEFAULT_INI, "couch.ini").

-export([start_link/1,stop/0]).

%% supervisor callbacks
-export([init/1]).

start_link(IniFilename) ->
	case whereis(couch_server_sup) of
	undefined ->
		start_server(IniFilename);
	_Else ->
		{error, already_started}
	end.
	
start_server(IniFilename) ->
	{ok, IniBin} =
	case IniFilename of
	"" ->
		% no ini file specified, check the command line args
		case init:get_argument(couchini) of
		{ok, [Filename]} ->
			file:read_file(Filename);
		_Else ->
			get_default_ini()
		end;
	IniFilename ->
		% if IniFilename is specified, it is used
		file:read_file(IniFilename)
	end,
	{ok, Ini} = couch_util:parse_ini(binary_to_list(IniBin)),
	ConsoleStartupMsg = couch_util:ini_get_value(Ini, "Couch", "ConsoleStartupMsg", "CouchDb server starting"),
	ConsoleStartupCompleteMsg = couch_util:ini_get_value(Ini, "Couch", "ConsoleStartupCompleteMsg", "CouchDb server started"),
	LogLevel = list_to_atom(couch_util:ini_get_value(Ini, "Couch", "LogLevel", "error")),
	DbRootDir = couch_util:abs_pathname(couch_util:ini_get_value(Ini, "Couch", "DbRootDir", ".")),
	HttpConfigFile = couch_util:abs_pathname(couch_util:ini_get_value(Ini, "Couch", "HttpConfigFile", "couch_httpd.conf")),	
	LogFile = couch_util:abs_pathname(couch_util:ini_get_value(Ini, "Couch", "LogFile", "couchdb.log")),
	UtilDriverDir = couch_util:abs_pathname(couch_util:ini_get_value(Ini, "Couch", "UtilDriverDir", ".")),
	UpdateNotifierExes = [couch_util:abs_pathname(UpdateNotifierExe) || {_,UpdateNotifierExe} <-  couch_util:ini_get_values_starting(Ini, "Couch", "DbUpdateNotificationProcess")],
	
	UpdateNotifierExes = [couch_util:abs_pathname(QueryExe) || {{"Couch", "DbUpdateNotificationProcess"}, QueryExe} <- Ini],
	
	QueryServers = [{Lang, couch_util:abs_pathname(QueryExe)} || {{"Couch Query Servers", Lang}, QueryExe} <- Ini],
	
	FtSearchQueryServer0 = couch_util:ini_get_value(Ini, "Couch", "FullTextSearchQueryServer", ""),
	
	FtSearchQueryServer =
	case FtSearchQueryServer0 of
		"" -> 	"";
		_ -> couch_util:abs_pathname(FtSearchQueryServer0)
	end,
		
	
	io:format("couch ~s (LogLevel=~s)~n", [couch_server:get_version(), LogLevel]),
	
	io:format("~s~n", [ConsoleStartupMsg]),
	
	StartResult = supervisor:start_link({local, couch_server_sup}, couch_server_sup, {LogFile, LogLevel, DbRootDir, QueryServers, HttpConfigFile, UtilDriverDir, UpdateNotifierExes, FtSearchQueryServer}),
	
	couch_log:debug("Startup Info:~n\tDbRootDir=~p~n" ++
		"\tHttpConfigFile=~p~n" ++ 
		"\tLogFile=~p~n" ++ 
		"\tUtilDriverDir=~p~n" ++
		"\tDbUpdateNotificationProcesses=~p~n" ++ 
		"\tFullTextSearchQueryServer=~p~n" ++
		"~s~n",
			[DbRootDir,
			HttpConfigFile,
			LogFile,
			UtilDriverDir,
			UpdateNotifierExes,
			FtSearchQueryServer,
			[lists:flatten(io_lib:format("\t~s=~p~n", [Lang, QueryExe])) || {Lang, QueryExe} <- QueryServers]]),
    
    case StartResult of
	{ok,_} ->
		% only output when startup was successful
		io:format("~s~n", [ConsoleStartupCompleteMsg]);
	_ -> ok
	end,
	StartResult.
    
stop() ->
	catch exit(whereis(couch_server_sup), normal),
	couch_log:stop().

get_default_ini() ->
	case file:read_file(?DEFAULT_INI) of
	{ok, Ini} ->
		{ok, Ini};
	{error, enoent} ->
		{ok, <<>>} % default ini missing, just return a blank
	end.
	
init({LogFile, LogLevel, DbServerRoot, QueryServers, HttpConfigFile, UtilDriverDir, UpdateNotifierExes, FtSearchQueryServer}) ->
	{ok, {{one_for_one, 10, 3600}, 
			[{couch_log,
				{couch_log, start_link, [LogFile, LogLevel]},
				permanent,
				brutal_kill,
				worker,
				[couch_server]},
			{couch_server,
				{couch_server, sup_start_link, [DbServerRoot]},
				permanent,
				brutal_kill,
				worker,
				[couch_server]},
			{couch_util,
				{couch_util, start_link, [UtilDriverDir]},
				permanent,
				brutal_kill,
				worker,
				[couch_util]},
			{couch_query_servers,
				{couch_query_servers, start_link, [QueryServers]},
				permanent,
				brutal_kill,
				worker,
				[couch_query_servers]},
			{httpd,
				{httpd, start_link, [HttpConfigFile]},
				permanent,
				1000,
				supervisor,
				[httpd]},
			{couch_db_update_event,
				{gen_event, start_link, [{local, couch_db_update}]},
				permanent,
				1000,
				supervisor,
				dynamic}
			] ++
			lists:map(fun(UpdateNotifierExe) ->
				{UpdateNotifierExe,
					{couch_db_update_notifier, start_link, [UpdateNotifierExe]},
					permanent,
					1000,
					supervisor,
					[couch_db_update_notifier]}
				end, UpdateNotifierExes)
			++
			case FtSearchQueryServer of
			"" ->
				[];
			_ ->
				[{couch_ft_query,
					{couch_ft_query, start_link, [FtSearchQueryServer]},
					permanent,
					1000,
					supervisor,
					[httpd]}]
			end
			}}.
			
