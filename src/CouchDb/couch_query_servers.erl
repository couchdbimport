%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-module(couch_query_servers).
-behaviour(gen_server).

-export([start_link/1]).

-export([init/1, terminate/2, handle_call/3, handle_cast/2, handle_info/2,code_change/3,stop/0]).
-export([start_doc_map/2, map_docs/2, stop_doc_map/1]).

-export([test/0, test/1]).

-include("couch_db.hrl").

start_link(QueryServerList) ->
	gen_server:start_link({local, couch_query_servers}, couch_query_servers, QueryServerList, []).
	
stop() ->
	exit(whereis(couch_query_servers), close).
	
readline(Port) ->
	readline(Port, []).
	
readline(Port, Acc) ->
	receive
	{Port, {data, {noeol, Data}}} ->
		readline(Port, [Data|Acc]);
	{Port, {data, {eol, Data}}} ->
		lists:flatten(lists:reverse(Acc, Data));
	{Port, Err} ->
		throw({map_process_error, Err})
	end.

writeline(Port, String) ->
	true = port_command(Port, String ++ "\n").

% send command and get a response.
prompt(Port, String) ->
	writeline(Port, String),
	readline(Port).
	
start_doc_map(Lang, Functions) ->
	Port = 
	case gen_server:call(couch_query_servers, {get_port, Lang}) of
	{ok, Port0} ->
		link(Port0),
		Port0;
	{empty, Cmd} ->
		couch_log:info("Spawing new ~s instance.", [Lang]),
		open_port({spawn, Cmd}, [stream, {line, 1000}, exit_status, hide, {cd, filename:dirname(Cmd)}]);
	Error ->
		throw(Error)
	end,
	"true" = prompt(Port, "[\"reset\"]"),
	% send the functions as json strings
	lists:foreach(fun(FunctionSource) ->
			case prompt(Port, "[\"add_map_fun\"," ++ cjson:encode(FunctionSource) ++ "]") of
			"true" -> ok;
			ErrorJsonString ->
				ErrorString = cjson:decode(ErrorJsonString),
				throw({error_compiling_map_function, ErrorString})
			end
		end,
		Functions),
	{ok, {Lang, Port}}.
	
map_docs({_Lang, Port}, Docs) ->		
	% send the documents
	Results =
	lists:map(
		fun(Doc) ->
			Json = cjson:encode(couch_doc:to_json_obj(Doc, false)),
			ResultsStr = prompt(Port, "[\"map_doc\"," ++ Json ++ "]"),
			Results = cjson:decode(ResultsStr),
			lists:map(
				fun(Result) ->
					case  obj_to_key_value(Result) of
					{ok, KV} ->
						[KV];
					0 ->
						[]; % not a match
					{obj, [{"multi", List}]} ->
						% list of key values
						lists:reverse(lists:map(
							fun(Result2) ->
								case obj_to_key_value(Result2) of
								{ok, KV} ->
									KV;
								_ ->
									{nil, Result2}
								end
							end,
							tuple_to_list(List)));
					ElseValue ->
						[{nil, ElseValue}]
					end
				end,
				tuple_to_list(Results))
		end,
		Docs),
	{ok, Results}.

% returns {ok, {Key,Value}} if the input is an object with just a key and/or value
% member, otherwise returns the input unchanged
obj_to_key_value({obj, PropList}) ->
	case PropList of
	[{"key", Key}, {"value", Value}] ->
		{ok, {Key, Value}};
	[{"value", Value}, {"key", Key}] ->
		{ok, {Key, Value}};
	[{"key", Key}] ->
		{ok, {Key, nil}};
	[{"value", Value}] ->
		{ok, {nil, Value}};
	_ ->
		{obj, PropList}
	end;
obj_to_key_value(Other) ->
	Other.
	

stop_doc_map(nil) ->
	ok;
stop_doc_map({Lang, Port}) ->
	ok = gen_server:call(couch_query_servers, {return_port, {Lang, Port}}),
	true = unlink(Port),
	ok.

init(QueryServerList) ->
	{ok, {QueryServerList, []}}.

terminate(_Reason, _Server) ->
	ok.
	
	
handle_call({get_port, Lang}, {FromPid, _}, {QueryServerList, LangPorts}) ->
	case lists:keysearch(Lang, 1, LangPorts) of
	{value, {_, Port}} ->
		true = port_connect(Port, FromPid),
		true = unlink(Port),
		{reply, {ok, Port}, {QueryServerList, lists:keydelete(Lang, 1, LangPorts)}};
	false ->
		case lists:keysearch(Lang, 1, QueryServerList) of
		{value, {_, ServerCmd}} ->
			{reply, {empty, ServerCmd}, {QueryServerList, LangPorts}};
		false -> % not a supported language
			{reply, {query_language_unknown, Lang}, {QueryServerList, LangPorts}}
		end
	end;
handle_call({return_port, {Lang, Port}}, _From, {QueryServerList, LangPorts}) ->
	true = port_connect(Port, self()),
	{reply, ok, {QueryServerList, [{Lang, Port} | LangPorts]}}.
	
handle_cast(_Whatever, {Cmd, Ports}) ->
	{noreply, {Cmd, Ports}}.
	
handle_info({Port, {exit_status, Status}}, {QueryServerList, LangPorts}) ->
	case lists:keysearch(Port, 2, LangPorts) of
	{value, {Lang, _}} ->
		case Status of
		0 -> ok;
		_ -> couch_log:error("Abnormal shutdown of ~s query server process (exit_status: ~w).", [Lang, Status])
		end,
		{noreply, {QueryServerList,  lists:keydelete(Port, 2, LangPorts)}};
	_ ->
		couch_log:error("Unknown linked port/process crash: ~p", [Port])
	end;
handle_info(_Whatever, {Cmd, Ports}) ->
	{noreply, {Cmd, Ports}}.
	
code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

test() ->
	test("../js/js -f main.js").
	
test(Cmd) ->	
	start_link(Cmd),
	{ok, DocMap} = start_doc_map("javascript", ["function(doc) {if (doc[0] == 'a') return doc[1];}"]),
	{ok, Results} = map_docs(DocMap, [#doc{body={"a", "b"}}, #doc{body={"c", "d"}},#doc{body={"a", "c"}}]),
	io:format("Results: ~w~n", [Results]),
	stop_doc_map(DocMap),
	ok.
		