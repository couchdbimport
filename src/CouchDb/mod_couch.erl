%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-module(mod_couch).

-include("couch_db.hrl").

-export([do/1, load/2]).

-include_lib("inets/src/httpd.hrl").

-record(uri_parts,
	{db = "",
	doc = "",
	resource = "",
	querystr = ""}).

-record(doc_query_args,
	{full=false,
	options = [],
	revs = [],
	all_revs = false,
	bin = ""
	}).

%% do. This is the main entry point into CouchDb from the HTTP server
	
do(ModData) ->
	#mod{request_uri=Uri,request_line=Request, parsed_header=Header,entity_body=Body} = ModData,
	case Uri of
	"/_utils/" ++ RestURI ->
		% if the URI is the utils directory, then this
		% tells mod_get (a std HTTP module) where to serve the file from 
		DocumentRoot = httpd_util:lookup(ModData#mod.config_db, document_root, ""),
		{Path, AfterPath} = httpd_util:split_path(DocumentRoot ++ "/" ++ RestURI),
		case filelib:is_file(Path) of
		true ->
			{proceed, [{real_name, {Path, AfterPath}} | ModData#mod.data]};
		false ->
			case filelib:is_dir(Path) of
			true ->
				% this ends up causing a "Internal Server Error", need to fix.
				{proceed, [{response,{403,"Forbidden"}}]};
			false ->
				{proceed, [{response,{404,"Not found"}}]}
			end
		end;
	"/favicon.ico" ->
		DocumentRoot = httpd_util:lookup(ModData#mod.config_db, document_root, ""),
		RealName = DocumentRoot ++ "/" ++ Uri,
		{Path, AfterPath} = httpd_util:split_path(RealName),
		{proceed, [{real_name, {Path, AfterPath}} | ModData#mod.data]};
	_ ->
		couch_log:info("HTTP Request: ~s~nHeader:~p", [Request, Header]),
		couch_log:debug("Body:~P", [Body, 100]),
		Parts = parse_uri(Uri),
		{ok, ResponseCode} =
			case Parts of
			#uri_parts{db="_all_dbs"} ->
				send_all_dbs(ModData, Parts);
			#uri_parts{db=""} ->
				% empty request
				show_couch_welcome(ModData);	
			_Else ->
				case (catch handle_request(ModData, Parts)) of
				{ok, Response} ->
					{ok, Response};
				{error_response, Error} ->
					send_error(ModData, Error);
				Error ->
					send_error(ModData, Error)
				end
			end,
		couch_log:info("HTTP Response Code:~p~n", [ResponseCode]),
		{proceed, [{response, {already_sent, ResponseCode, 0}} | ModData#mod.data]}
	end.
	
parse_uri(RequestUri) ->
	% seperate out the path and query portions and
	% strip out leading slash and question mark.
	case httpd_util:split_path(RequestUri) of
	{[$/|UriPath], [$?|QueryStr]}	-> ok;
	{[$/|UriPath], QueryStr}		-> ok % there is no query
	end,
	
	case UriPath of
	"" ->
		#uri_parts{querystr=QueryStr};	
	UriPath ->
		case lists:last(UriPath) of
		$/ -> % ends with a slash means database only
			DbId = lists:sublist(UriPath, length(UriPath) - 1), % strip off trailing slash
			#uri_parts{db=DbId,querystr=QueryStr};
		_Else ->
			% lets try to parse out the UriPath
			{ok, SplitUrl} = regexp:split(UriPath, "/"),
			case SplitUrl of
			[DbId] -> % single element list.
				#uri_parts{db = DbId, querystr = QueryStr};
			_Else2 ->
				% multi element list. treat last element as the docid
				[DocIdDocResource | RevDbPath] = lists:reverse(SplitUrl),
				DbId = couch_util:implode(lists:reverse(RevDbPath), "/"),
				% Everything before first ":" is the docId,
				% and everything after is the resource id.
				{ok, [DocId | DocResourceList]} = regexp:split(DocIdDocResource, ":"), 
				ResourceId = couch_util:implode(DocResourceList, ":"),
				#uri_parts{db=DbId, doc=DocId, resource=ResourceId, querystr=QueryStr}
			end
		end
	end.

% return json doc header values list	
resp_json_header(ModData) ->
	resp_header(ModData, "text/plain; charset=utf-8").

% return doc header values list
resp_header(#mod{http_version=Version}, ContentType) ->
	[{"cache-control", "no-cache"},
	{"pragma", "no-cache"},
	{"expires", httpd_util:rfc1123_date()},
	{"damien", "awesome"},
	{"content-type", ContentType},
	case Version == "HTTP/1.1" of
	true ->
		{"transfer-encoding", "chunked"};
	false ->
		{"connection", "close"}
	end].
	

url_decode([$%, Hi, Lo | Tail]) ->
    Hex = erlang:list_to_integer([Hi, Lo], 16),
    xmerl_ucs:to_utf8([Hex]) ++ url_decode(Tail);
url_decode([H|T]) ->
    [H |url_decode(T)];
url_decode([]) ->
    [].


send_header(ModData, RespCode, Headers) ->
	 couch_log:debug("HTTP response headers (code ~w):~p", [RespCode, Headers]),
	httpd_response:send_header(ModData, RespCode, Headers).
	
send_chunk(ModData, Data) ->
	httpd_response:send_chunk(ModData, Data, false).

send_final_chunk(ModData) ->
	httpd_response:send_final_chunk(ModData, false).
	
show_couch_welcome(ModData) ->
	send_header(ModData, 200, resp_json_header(ModData)),
	send_chunk(ModData, "{\"couchdb\": \"Welcome\", "),
	send_chunk(ModData, "\"version\": \"" ++ couch_server:get_version()),
	send_chunk(ModData, "\"}"),
	send_final_chunk(ModData),
	{ok, 200}.

handle_request(#mod{method="POST"}=ModData, #uri_parts{db=DbName, doc="_missing_revs"}=UriParts) ->
	handle_missing_revs_request(ModData, UriParts,  open_db(DbName));
handle_request(#mod{method="POST"}=ModData, #uri_parts{db=DbName, doc=""}=UriParts) ->
	% POST with no doc specified, therefore document addition
	handle_db_request(ModData#mod{method="PUT"}, UriParts,  open_db(DbName));
handle_request(#mod{method="PUT"}=ModData, #uri_parts{db=DbName, doc=""}=_UriParts) ->
	% put with no doc specified, therefore database creation
	case couch_server:create(DbName, []) of
	{ok, _Db} ->
		send_ok(ModData, 201);
	{error, database_already_exists} ->
		Msg = io_lib:format("Database ~p already exists.", [DbName]),
		throw({error_response, {database_already_exists, Msg}});
	Error ->		
		Msg = io_lib:format("Error creating database ~p: ~p", [DbName, Error]),
		throw({error_response, {unknown_error, Msg}})
	end;
handle_request(#mod{method="DELETE"}=ModData, #uri_parts{db=DbName, doc=""}=_UriParts) ->
	% delete with no doc specified, therefore database delete
	case couch_server:delete(DbName) of
	ok ->
		send_ok(ModData, 202);
	Error ->
		throw({error_response, Error})
	end;
handle_request(ModData, #uri_parts{db=DbName}=UriParts) ->
	handle_db_request(ModData, UriParts,  open_db(DbName)).
	
% returns db, otherwise throws exception. Note: no {ok,_}.
open_db(DbName) ->
	case couch_server:open(DbName) of
	{ok, Db} ->
		Db;
	Error ->
		throw({error_response, Error})
	end.

handle_missing_revs_request(#mod{entity_body=RawJson}=ModData, _UriParts, Db) ->
	{obj, DocIdRevs} = cjson:decode(RawJson),
	{ok, Results} = couch_db:get_missing_revs(Db, tuple_to_list(DocIdRevs)),
	send_ok(ModData, 200, {obj, [{missing_revs, list_to_tuple(Results)}]}).
	
	

handle_db_request(#mod{method="POST", entity_body=RawBody, parsed_header=Headers}=ModData, #uri_parts{doc="_temp_view"}=Uri, Db) ->
	Type = proplists:get_value(Headers, "content-type", "text/javascript"),
	handle_db_request(ModData#mod{method="GET"}, Uri#uri_parts{resource=Type ++ "|" ++ RawBody}, Db);
handle_db_request(#mod{method="DELETE"}=ModData, #uri_parts{doc=DocId,querystr=QueryStr}, Db) ->
	% handle doc deletion
	#doc_query_args{revs=Revs} = doc_parse_query(QueryStr),
	
	Revs2 = case Revs of [] -> all; _ -> Revs end,
	
	case couch_db:delete_doc(Db, DocId, Revs2) of
	{ok, [{ok, NewRev}]} ->	
		send_ok(ModData, 202, [{"_rev",NewRev}]); % single doc success
	{conflict, _Rev} ->	
		throw({error_response, conflict}); % single doc conflict failure
	{ok, Results} ->
		% multiple results, send results as list
		Results2 = 
		lists:map(fun(Result) ->
				case Result of
				{ok, NewRev} ->
					NewRev;
				{conflict, _Rev} ->
					"conflict"
				end
			end, Results),
		send_ok(ModData, 202, [{multi, list_to_tuple(Results2)}]);
	Error ->
		throw({error_response, Error})
	end;
handle_db_request(#mod{method="PUT",entity_body=RawBody}=ModData, #uri_parts{doc=UrlDocId,querystr=QueryStr}, Db) ->
	% handle doc creation
	% just assume the content
	% is application/json utf-8
	
	#doc_query_args{options=SaveOptions} = doc_parse_query(QueryStr),
	couch_log:debug("Body:~100s", [RawBody]),

	Json = cjson:decode(RawBody),
	case Json of 
	{obj, [{"_bulk_docs", SubJsonDocs}]} ->	
			% convert all the doc elements to native docs
			DocsAndOptions =
				lists:foldl(
					fun(SubDoc, DocAcc) ->
						NewDoc = couch_doc:from_json_obj(SubDoc),
						NewDoc2 = NewDoc#doc{uuid=
							case NewDoc#doc.uuid of
							[] -> couch_util:new_uuid();
							Id -> Id
							end}, 
						[{[NewDoc2], [new_edits| SaveOptions]} | DocAcc]
					end,
					[], tuple_to_list(SubJsonDocs)),
			% save them
			{ok, Results} = couch_db:save_docs(Db, DocsAndOptions),
			DocResults = 
			lists:zipwith(fun([Result], {[Doc],_}) ->
					case Result of
					{ok, RevId} ->
						{obj, [{"ok",true}, {"_id", Doc#doc.uuid}, {"_rev", RevId}]};
					Error ->
						{JsonError, _HttpCode} = error_to_json(Error),
						JsonError
					end
				end, Results, DocsAndOptions),

			send_ok(ModData, 201, [{results, list_to_tuple(DocResults)}], []);
	_ ->
		Doc = couch_doc:from_json_obj(Json),
		%if the docid is specified in the URL, use that (we override the
		% json document's _id member).
		Doc2 =
			case {UrlDocId, Doc#doc.uuid} of
			{"", ""} ->   % both blank
				Doc#doc{uuid = couch_util:new_uuid()};
			{"", _} ->    % url is blank, doc's id is not, use that
				Doc;
			{_, _}  ->    % both are available, prefer the Url one 
				Doc#doc{uuid = UrlDocId}
			end,
		{obj, ObjProps} = Json,
		Doc3 =
			case proplists:get_value("_rev", ObjProps) of
			undefined ->
				Doc2;
			PrevRev ->
				% there is a "based on" revisionid
				% load the document from the database and see if its the same
				% revision. If so then snag the revision history and use it.
				case couch_db:open_doc(Db, Doc2#doc.uuid, [allow_stub]) of
				{ok, DiskDoc} ->
					case PrevRev == lists:nth(1, DiskDoc#doc.revisions) of
					true ->
						Doc2#doc{revisions = DiskDoc#doc.revisions};
					false ->
						% oops, the revision in db isn't what this doc is based
						% upon, a conflict!
						throw({error_response, {update_error, conflict}})
					end;
				{not_found, missing} ->	
					throw({error_response, {update_error, missing_rev}})
				end
			end,
		case couch_db:save_doc(Db, Doc3, SaveOptions) of
		{ok, NewRevId} ->
			Props = [{"_id", Doc3#doc.uuid}, {"_rev", NewRevId}],
			HeaderInfo = [{"x-couch-id", Doc3#doc.uuid}, {"x-couch-rev", integer_to_list(NewRevId)}],
			send_ok(ModData, 201, Props, HeaderInfo);
		Error ->
			throw({error_response, {update_error, Error}})
		end
	end;
handle_db_request(#mod{method="GET"}=ModData,
		#uri_parts{doc=DocId, resource=Resource}=UriParts, Db) ->
	% handle doc retrieval
	case {DocId, Resource} of
	{"", ""} ->
		send_database_info(ModData, UriParts, Db);
	{"_all_docs", ""} ->
		send_all_docs(ModData, UriParts, Db);
	{"_all_docs_by_update_seq", ""} ->
		send_all_docs_by_seq(ModData, UriParts, Db);
	{_, ""} ->
		send_doc(ModData, UriParts, Db);
	{?DESIGN_DOC_PREFIX ++ _, _ViewId} ->
		send_view(ModData, UriParts, Db);
	{"_temp_view", _ViewId} ->
		send_view(ModData, UriParts, Db);
	{_, _ViewId} ->
		throw({error_response, {error, not_a_design_doc}})
	end.

doc_parse_query(QueryStr) ->
	QueryList = httpd:parse_query(QueryStr),
	lists:foldl(fun({Key,Value}, Args) ->
			case {Key, Value} of
			{"full", "true"} ->
				Args#doc_query_args{full=true};
			{"latest", "true"} ->
				Options = [latest | Args#doc_query_args.options],
				Args#doc_query_args{options=Options};
			{"rev", RevIdStr} ->
				Revs = [list_to_integer(RevIdStr) | Args#doc_query_args.revs],
				Args#doc_query_args{revs=Revs};
			{"all_revs", "true"} ->
				Args#doc_query_args{revs=all};
			{"new_edits", "true"} ->
				Options = [new_edits | Args#doc_query_args.options],
				Args#doc_query_args{options=Options};				
			{"attachment", BinName} ->
				Args#doc_query_args{bin=BinName};
			{"delay_commit", "true"} ->
				Options = [delay_commit | Args#doc_query_args.options],
				Args#doc_query_args{options=Options};
			_Else -> % unknown key value pair, ignore.
				Args
			end
		end,
		#doc_query_args{}, QueryList).
	
send_database_info(ModData, #uri_parts{db=DbName}, Db) ->
	{ok, InfoList} = couch_db:get_info(Db),
	ok = send_header(ModData, 200, resp_json_header(ModData)),
	DocCount = proplists:get_value(doc_count, InfoList),
	LastUpdateSequence = proplists:get_value(last_update_seq, InfoList),
	ok = send_chunk(ModData, "{\"db_name\": \"" ++ DbName ++ "\", \"doc_count\":" ++ integer_to_list(DocCount) ++ ", \"update_seq\":" ++ integer_to_list(LastUpdateSequence)++"}"),
	ok = send_final_chunk(ModData),
	{ok, 200}.
	
send_doc(ModData, #uri_parts{doc=DocId,querystr=QueryStr}, Db) ->	
	#doc_query_args{full=Full, options=OpenOptions, revs=Revs, bin=BinName} = doc_parse_query(QueryStr),
	case BinName of		
	"" ->
		case Revs of
		[] ->
			case couch_db:open_doc(Db, DocId, OpenOptions) of
			{ok, Doc} ->
				send_json(ModData, 200, couch_doc:to_json_obj(Doc, Full));
			Error ->
				throw({error_response, Error})
			end;
		Revs ->
			{ok, Results} = couch_db:open_doc_revs(Db, DocId, Revs, OpenOptions),
			ok = send_header(ModData, 200, resp_json_header(ModData)),
			ok = send_chunk(ModData, "{\"docs\":["),
			% We loop through the docs. The first time through the seperator
			% is whitespace, then a comma on subsequent iterations.  
			lists:foldl(
				fun(Result, AccSeperator) ->
					case Result of
					{ok, Doc} ->
						JsonDoc= couch_doc:to_json_obj(Doc, Full),
						ok = send_chunk(ModData, AccSeperator ++ lists:flatten(cjson:encode(JsonDoc)));
					{{not_found, missing}, RevId} ->
						Json = {obj, [{id, DocId}, {rev, RevId}, {missing, true}]},
						ok = send_chunk(ModData, AccSeperator ++ lists:flatten(cjson:encode(Json)))
					end,
					",\n" % AccSeperator now has a comma
				end,
				"\n", Results),
			ok = send_chunk(ModData, "\n]}"),
			ok = send_final_chunk(ModData),
			{ok, 200}
		end;
	BinName ->
		case couch_db:open_doc(Db, DocId, OpenOptions) of
		{ok, #doc{attachments=Bins}} ->
			case proplists:lookup(BinName,Bins) of
			none ->
				throw({error_response, {not_found, missing}});
			{_, {"base64", Bin}} ->
				Suffix = httpd_util:suffix(BinName),
				MimeType = httpd_util:lookup_mime_default(ModData#mod.config_db,
						      Suffix,"text/plain"),
				ok = send_header(ModData, 200, resp_header(ModData, MimeType) ++ [{"content-length", integer_to_list(couch_doc:bin_size(Bin))}]),
				couch_doc:bin_foldl(Bin,
						fun(BinSegment, []) ->
							ok = send_chunk(ModData, BinSegment),
							{ok, []}
						end,
						[]),
				ok = send_final_chunk(ModData),
				{ok, 200};
			{_, {_, Bin}} ->
				% for now, anything else, send as json
				Json = binary_to_term(couch_doc:bin_to_binary(Bin)),
				send_json(ModData, 200, Json)
			end;
		Error ->
			throw({error_response, Error})
		end
	end.

send_json(ModData, Code, JsonData) ->	
	send_json(ModData, Code, JsonData, []).
	
send_json(ModData, Code, JsonData, AdditionalHeaders) ->
	ok = send_header(ModData, Code, resp_json_header(ModData) ++ AdditionalHeaders),
	ok = send_chunk(ModData, lists:flatten(cjson:encode(JsonData))),
	ok = send_final_chunk(ModData),
	{ok, Code}.


send_ok(ModData, Code) ->	
		send_ok(ModData, Code, []).

send_ok(ModData, Code, AdditionalProps) ->	
	send_ok(ModData, Code, AdditionalProps, []).
	
send_ok(ModData, Code, AdditionalProps, AdditionalHeaders) ->
	send_json(ModData, Code, {obj, [{ok, true}|AdditionalProps]}, AdditionalHeaders).


-record(query_args,
	{start_key = nil,
	end_key = <<>>,
	count = 10000000000,	% a huge huge default number. Picked so we don't have
							% to do different logic for when there is no count limit
	update = true,
	direction = fwd,
	start_docid = nil,
	end_docid = <<>>,
	skip = 0
	}).
	
reverse_key_default(nil) -> <<>>;
reverse_key_default(<<>>) -> nil;
reverse_key_default(Key) -> Key.
	
view_parse_query(QueryStr) ->
	QueryList = httpd:parse_query(QueryStr),
	lists:foldl(fun({Key,Value}, Args) ->
			case {Key, Value} of
			{"key", Value} ->
				JsonKey = cjson:decode(url_decode(Value)),
				Args#query_args{start_key=JsonKey,end_key=JsonKey};
			{"startkey_docid", DocId} ->
				Args#query_args{start_docid=DocId};
			{"startkey", Value} ->
				Args#query_args{start_key=cjson:decode(url_decode(Value))};		
			{"endkey", Value} ->
				Args#query_args{end_key=cjson:decode(url_decode(Value))};
			{"count", Value} ->
				case (catch list_to_integer(Value)) of
				Count when is_integer(Count) ->
					Args#query_args{count=Count};
				_Error ->
					Msg = io_lib:format("Bad URL query value, number expected: count=~s", [Value]),
					throw({error_response, {query_parse_error, Msg}})
				end;
			{"update", "false"} ->
				Args#query_args{update=false};
			{"reverse", "true"} ->
				case Args#query_args.direction of
				fwd ->
					Args#query_args {
						direction = rev,
						start_key = reverse_key_default(Args#query_args.start_key),
						start_docid = reverse_key_default(Args#query_args.start_docid),
						end_key = reverse_key_default(Args#query_args.end_key),
						end_docid =  reverse_key_default(Args#query_args.end_docid)};
				_ ->
					Args %already reversed
				end;
			{"skip", Value} ->
				case (catch list_to_integer(Value)) of
				Count when is_integer(Count) ->
					Args#query_args{skip=Count};
				_Error ->
					Msg = io_lib:format("Bad URL query value, number expected: skip=~s", [Value]),
					throw({error_response, {query_parse_error, Msg}})
				end;
			_Else -> % unknown key value pair, ignore.
				Args
			end
		end,
		#query_args{}, QueryList).
	
	
make_view_fold_fun(ModData, #uri_parts{doc=DocId, resource=ViewId}, QueryArgs) ->
	#query_args{
		end_key=EndKey,
		end_docid=EndDocId,
		direction=Dir
		} = QueryArgs,
	
	fun({Uuid, Rev}, Key, Value, Offset, TotalViewCount, {AccCount, AccSkip, HeaderSent}) ->
		PassedEnd =
		case Dir of
		fwd ->
			couch_view_group:less_json({EndKey,EndDocId}, {Key, Uuid});
		rev ->
			couch_view_group:less_json({Key, Uuid}, {EndKey,EndDocId})
		end,
		case PassedEnd of
		true ->
			% The stop key has been passed, stop looping.
			{stop, {AccCount, AccSkip, HeaderSent}};
		false ->
			case AccCount > 0 of
			true ->
				case AccSkip > 0 of
				true ->
					{ok, {AccCount, AccSkip - 1, HeaderSent}};
				false ->
					case HeaderSent of
					header_sent ->
						JsonObj = {obj, [{"_id", Uuid}, {"_rev", Rev} | key_value_to_props(Key,Value)]},
						ok = send_chunk(ModData, ",\n" ++  lists:flatten(cjson:encode(JsonObj)));
					_Else ->
						% We do this the first time through, NOT before folding,
						% because we want to make sure the view exists efficiently.
						ok = send_header(ModData, 200, resp_json_header(ModData)),
						FullViewId = cjson:encode(lists:flatten(io_lib:format("~s:~s", [DocId, ViewId]))),
						JsonBegin =	io_lib:format("{\"view\":~s,\"total_rows\":~w, \"offset\":~w, \"rows\":[\n",
								[FullViewId, TotalViewCount, Offset]),
						JsonObj = {obj, [{"_id", Uuid}, {"_rev", Rev} | key_value_to_props(Key,Value)]},
						ok = send_chunk(ModData, lists:flatten(JsonBegin ++ cjson:encode(JsonObj)))
					end,
					{ok, {AccCount - 1, 0, header_sent}}
				end;
			false ->
				{stop, {0, 0, HeaderSent}} % we've done "count" rows, stop foldling
			end
		end
	end.
	
key_value_to_props(Key,Value) ->
	case Key of nil -> []; _-> [{"key", Key}] end ++
	case Value of nil -> []; _-> [{"value", Value}] end.


send_view(ModData, #uri_parts{doc=DocId, resource=ViewId, querystr=QueryStr}=UriParts, Db) ->	
	QueryArgs = view_parse_query(QueryStr),
	#query_args{
		start_key=StartKey,
		count=Count,
		skip=SkipCount,
		update=Update,
		direction=Dir,
		start_docid=StartDocId} = QueryArgs,
	case Update of
	true ->
		UpdateResult =
		case DocId of
		"_temp_view" ->
			couch_db:update_temp_view_group_sync(Db, ViewId);
		_ ->
			couch_db:update_view_group_sync(Db, DocId)
		end,
		case UpdateResult of
		ok ->		ok;
		Error ->	throw({error_response, Error})
		end;
	false ->
		ok
	end,
	FoldlFun = make_view_fold_fun(ModData, UriParts, QueryArgs),
	FoldResult = 
		case DocId of
		"_temp_view" ->
			couch_db:fold_temp_view(Db, ViewId, {StartKey, StartDocId}, Dir, FoldlFun, {Count, SkipCount, header_not_sent});
		_ ->
			couch_db:fold_view(Db, DocId, ViewId, {StartKey, StartDocId}, Dir, FoldlFun, {Count, SkipCount, header_not_sent})
		end,
	case FoldResult of
	{ok, TotalRows, {_, _, header_not_sent}} ->
		% nothing found in the view, nothing has been returned
		% send empty view
		ok = send_header(ModData, 200, resp_json_header(ModData)),
		FullViewId = lists:flatten(cjson:encode(lists:flatten(io_lib:format("~s:~s", [DocId, ViewId])))),
		JsonEmptyView = lists:flatten(
			io_lib:format("{\"view\":~s,\"total_rows\":~w,\"rows\":[]}",
				[FullViewId, TotalRows])),
		ok = send_chunk(ModData, JsonEmptyView),
		ok = send_final_chunk(ModData),
		{ok, 200};
	{ok, _TotalRows, {_, _, header_sent}} ->
		% end the view
		ok = send_chunk(ModData, "\n]}"),
		ok = send_final_chunk(ModData),
		{ok, 200};
	Error2 ->
		throw({error_response, Error2})
	end.

send_all_dbs(ModData, _Parts)->
	{ok, DbNames} = couch_server:all_databases(),
	ok = send_header(ModData, 200, resp_json_header(ModData)),
	ok = send_chunk(ModData, lists:flatten(cjson:encode(list_to_tuple(DbNames)))),
	ok = send_final_chunk(ModData),
	{ok, 200}.		
	
	
send_all_docs(ModData, #uri_parts{querystr=QueryStr}, Db) ->
	QueryArgs = view_parse_query(QueryStr),
	#query_args{
		start_key=StartKey,
		count=Count,
		direction=Dir} = QueryArgs,
	ok = send_header(ModData, 200, resp_json_header(ModData)),
	ok = send_chunk(ModData,
		"{\"view\":\"_all_docs\", \"rows\":[\n"),	
	couch_db:enum_docs(Db, StartKey, Dir,
		fun(#doc_info{uuid=Id, revision=Rev, deleted=Deleted}, {AccSeperator, AccCount}) ->	
			case {Deleted, AccCount > 0} of
			{false, true} ->
				Json = {obj, [{"_id", Id}, {"_rev", Rev}]} ,
				ok = send_chunk(ModData, AccSeperator ++ lists:flatten(cjson:encode(Json))),
				{ok, {",\n", AccCount - 1}};
			{true, true} ->
				{ok, {AccSeperator, AccCount}}; % skip
			{_, false} ->
				{stop, 0} % we've sent "count" rows, stop folding
			end
		end, {"", Count}),
	ok = send_chunk(ModData, "\n]}"),
	ok = send_final_chunk(ModData),
	{ok, 200}.

send_all_docs_by_seq(ModData, #uri_parts{querystr=QueryStr}, Db) ->
	QueryArgs = view_parse_query(QueryStr),
	#query_args{
		start_key=StartKey,
		count=Count,
		direction=Dir} = QueryArgs,
	ok = send_header(ModData, 200, resp_json_header(ModData)),
	ok = send_chunk(ModData,
		"{\"view\":\"_docs_by_update_seq\", \"docs\":[\n"),	

	couch_db:enum_docs_since(Db, list_to_integer(StartKey), Dir,
		fun(#doc_info{uuid=Id, revision=Rev, update_seq=UpdateSeq, deleted=Deleted, conflict_revs=ConflictRevs}, {AccSeperator, AccCount}) ->
			case AccCount of			
			0 ->
				{stop, 0}; % we've sent "count" rows, stop folding
			_ ->
				ConflictRevsProp = 
					case ConflictRevs of
					[]	->	[];
					_	->	[{conflicts, list_to_tuple(ConflictRevs)}]
					end,
				DeletedProp =
					case Deleted of
					true -> [{deleted, true}];
					false -> []
					end,
				Json = {obj,
							[{id, Id},
							{rev, Rev},
							{update_seq, UpdateSeq}] ++
							ConflictRevsProp ++ DeletedProp},
				ok = send_chunk(ModData, AccSeperator ++ lists:flatten(cjson:encode(Json))),
				{ok, {",\n", AccCount - 1}}
			end
		end, Count),
	ok = send_chunk(ModData, "\n]}"),
	ok = send_final_chunk(ModData),
	{ok, 200}.	
	
send_error(ModData, Error) ->
	{Json, Code} = error_to_json(Error),
	couch_log:info("HTTP Error (code ~w): ~p", [Code,  Json]),
	send_json(ModData, Code, Json).
	
	

% convert an error response into a json object and http error code.
error_to_json(Error) ->
	{HttpCode, Atom, Reason} = error_to_json0(Error),
	Reason1 =
	 	if
			is_list(Reason) -> lists:flatten(Reason);
			is_atom(Reason) -> atom_to_list(Reason);
			true 			-> lists:flatten(io_lib:write(Reason)) % else term to text
		end,
		
	Json =
		{obj,
			[{error, {obj,
						[{id, atom_to_list(Atom)},
						{reason, Reason1}]}}]},
	{Json, HttpCode}.

error_to_json0(not_found) ->
	{404, not_found, "missing"};
error_to_json0({not_found, Reason}) ->
	{404, not_found, Reason};
error_to_json0({database_already_exists, Reason}) ->
	{409, database_already_exists, Reason}; % 409, conflict error	
error_to_json0({update_error, conflict}) ->
	{409, conflict, conflict}; % 409, conflict error	
error_to_json0({Id, Reason}) when is_atom(Id) ->
	{500, Id, Reason};
error_to_json0(Error) ->
	{500, error, Error}.

%%
%% Configuration
%%

%% load

load("Foo Bar", []) ->
	{ok, [], {script_alias, {"foo", "bar"}}}.
	
	

