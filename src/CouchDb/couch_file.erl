%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-module(couch_file).
-behaviour(gen_server).

-export([open/2, close/1, pread/3, pwrite/3, expand/2, bytes/1, sync/1, truncate/2]).
-export([append_term/2, pread_term/2]).
-export([init/1, terminate/2, handle_call/3, handle_cast/2, code_change/3, handle_info/2]).

%%----------------------------------------------------------------------
%% Args:   Valid Options are [create] and [create,overwrite].
%%	Files are opened in read/write mode.
%% Returns: On success, {ok, Fd}
%%	or {error, Reason} if the file could not be opened.
%%----------------------------------------------------------------------

open(Filepath, Options) ->
	case gen_server:start_link(couch_file, {Filepath, Options, self()}, []) of
	{ok, FdPid} ->
		% we got back an ok, but that doesn't really mean it was successful.
		% Instead the true status has been sent back to us as a message.
		% We do this because if the gen_server doesn't initialize properly,
		% it generates a crash report that will get logged. This avoids
		% that mess, because we don't want.
		receive
		{FdPid, ok} ->
			{ok, FdPid};
		{FdPid, Error} ->
			Error
		end;
	Error ->
		Error
	end.
	
	
%%----------------------------------------------------------------------
%% Args:	Pos is the offset from the beginning of the file, Bytes is
%%	is the number of bytes to read.
%% Returns: {ok, Binary} where Binary is a binary data from disk
%%	or {error, Reason}.
%%----------------------------------------------------------------------

pread(Fd, Pos, Bytes) when Bytes > 0 ->
	gen_server:call(Fd, {pread, Pos, Bytes}).
	
	
%%----------------------------------------------------------------------
%% Args:	Pos is the offset from the beginning of the file, Bin is
%%	is the binary to write
%% Returns: ok
%%	or {error, Reason}.
%%----------------------------------------------------------------------
	
pwrite(Fd, Pos, Bin) ->
	gen_server:call(Fd, {pwrite, Pos, Bin}).
	
%%----------------------------------------------------------------------
%% Purpose:	To append a segment of zeros to the end of the file.
%% Args:	Bytes is the number of bytes to append to the file.
%% Returns: {ok, Pos} where Pos is the file offset to the beginning of
%%	the new segments.
%%	or {error, Reason}.
%%----------------------------------------------------------------------

expand(Fd, Bytes) when Bytes > 0 ->
	gen_server:call(Fd, {expand, Bytes}).


%%----------------------------------------------------------------------
%% Purpose:	To append an Erlang term to the end of the file.
%% Args:	Erlang term to serialize and append to the file.
%% Returns: {ok, Pos} where Pos is the file offset to the beginning the
%%	serialized	term. Use pread_term to read the term back.
%%	or {error, Reason}.
%%----------------------------------------------------------------------

append_term(Fd, Term) ->
	gen_server:call(Fd, {append_term, Term}).
	

%%----------------------------------------------------------------------
%% Purpose:	Reads a term from a file that was written with append_term
%% Args:	Pos, the offset into the file where the term is serialized.
%% Returns: {ok, Term}
%%	or {error, Reason}.
%%----------------------------------------------------------------------

pread_term(Fd, Pos) ->
	gen_server:call(Fd, {pread_term, Pos}).	


%%----------------------------------------------------------------------
%% Purpose:	The length of a file, in bytes.
%% Returns: {ok, Bytes}
%%	or {error, Reason}.
%%----------------------------------------------------------------------

% length in bytes
bytes(Fd) ->
	gen_server:call(Fd, bytes).

%%----------------------------------------------------------------------
%% Purpose:	Truncate a file to the number of bytes.
%% Returns: ok
%%	or {error, Reason}.
%%----------------------------------------------------------------------

truncate(Fd, Pos) ->
	gen_server:call(Fd, {truncate, Pos}).
	
%%----------------------------------------------------------------------
%% Purpose:	Ensure all bytes written to the file are flushed to disk.
%% Returns: ok
%%	or {error, Reason}.
%%----------------------------------------------------------------------

sync(Fd) ->
	gen_server:call(Fd, sync).

%%----------------------------------------------------------------------
%% Purpose: Close the file. Is performed asynchronously.
%% Returns: ok
%%----------------------------------------------------------------------
close(Fd) ->
	gen_server:cast(Fd, close).
	


init_status_ok(ReturnPid, Fd) ->
	ReturnPid ! {self(), ok}, % signal back ok
	{ok, Fd}.
	
init_status_error(ReturnPid, Error) ->
	ReturnPid ! {self(), Error}, % signal back error status
	self() ! self_close, % tell ourself to close async
	{ok, nil}.

% server functions

init({Filepath, Options, ReturnPid}) ->
	case lists:member(create, Options) of
	true ->
		filelib:ensure_dir(Filepath),
		case file:open(Filepath, [read, write, raw, binary]) of
		{ok, Fd} ->
			{ok, Length} = file:position(Fd, eof),
			case Length > 0 of
			true ->
				% this means the file already exists and has data.
				% FYI: We don't differentiate between empty files and non-existant
				% files here. That could cause issues someday.
				case lists:member(overwrite, Options) of
				true ->
					{ok, 0} = file:position(Fd, 0),
					ok = file:truncate(Fd),
					init_status_ok(ReturnPid, Fd);
				false ->
					ok = file:close(Fd),
					init_status_error(ReturnPid, {error, file_exists})
				end;
			false -> 
				init_status_ok(ReturnPid, Fd)
			end;
		Error ->
			init_status_error(ReturnPid, Error)
		end;
	false ->
		% open in read mode first, so we don't create the file if it doesn't exist.
		case file:open(Filepath, [read, raw]) of
		{ok, Fd_Read} ->
			{ok, Fd} = file:open(Filepath, [read, write, raw, binary]),
			ok = file:close(Fd_Read),
			init_status_ok(ReturnPid, Fd);
		Error ->
			init_status_error(ReturnPid, Error)
		end
	end.
	

terminate(_Reason, nil) ->
	ok;
terminate(_Reason, Fd) ->
	file:close(Fd),
	ok.
	
	
handle_call({pread, Pos, Bytes}, _From, Fd) ->
	{reply, file:pread(Fd, Pos, Bytes), Fd};	
handle_call({pwrite, Pos, Bin}, _From, Fd) ->
	{reply, file:pwrite(Fd, Pos, Bin), Fd};	
handle_call({expand, Num}, _From, Fd) ->
	{ok, Pos} = file:position(Fd, eof),
	{reply, {file:pwrite(Fd, Pos + Num - 1, <<0>>), Pos}, Fd};	
handle_call(bytes, _From, Fd) ->
	{reply, file:position(Fd, eof), Fd};	
handle_call(sync, _From, Fd) ->
	{reply, file:sync(Fd), Fd};	
handle_call({truncate, Pos}, _From, Fd) ->
	{ok, Pos} = file:position(Fd, Pos),
	{reply, file:truncate(Fd), Fd};
handle_call({append_term, Term}, _From, Fd) ->
	Bin = term_to_binary(Term),
	TermLen = size(Bin),
	Bin2 = <<TermLen:32, Bin/binary>>,
	{ok, Pos} = file:position(Fd, eof),
	{reply, {file:pwrite(Fd, Pos, Bin2), Pos}, Fd};
handle_call({pread_term, Pos}, _From, Fd) ->
	{ok, <<TermLen:32>>}
		= file:pread(Fd, Pos, 4),
	{ok, Bin} = file:pread(Fd, Pos + 4, TermLen),
	{reply, {ok, binary_to_term(Bin)}, Fd}.
	
	
handle_cast(close, Fd) ->
	{stop,normal,Fd}. % causes terminate to be called

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

handle_info(self_close, State) ->
	{stop,normal,State};
handle_info(_Info, State) ->
	{noreply, State}.