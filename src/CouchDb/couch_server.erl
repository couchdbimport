%% CouchDb
%% Copyright (C) 2006  Damien Katz
%% 
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License
%% as published by the Free Software Foundation; either version 2
%% of the License, or (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program; if not, write to the Free Software
%% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-module(couch_server).
-behaviour(gen_server).
-behaviour(application).

-export([start/0,start/1,start/2,stop/0,stop/1]).
-export([open/1,create/2,delete/1,all_databases/0,get_version/0]).
-export([init/1, handle_call/3,sup_start_link/1]).
-export([handle_cast/2,code_change/3,handle_info/2,terminate/2]).

-include("couch_db.hrl").

-record(server,{
	root_dir = [],
	dbname_regexp
	}).
	
start() ->
	start("").
	
start(IniFile) when is_atom(IniFile) ->
	couch_server_sup:start_link(atom_to_list(IniFile) ++ ".ini");
start(IniNum) when is_integer(IniNum) ->
	couch_server_sup:start_link("couch" ++ integer_to_list(IniNum) ++ ".ini");
start(IniFile) ->
	couch_server_sup:start_link(IniFile).
	
start(_Type, _Args) ->
	start().
	
stop() ->
	couch_server_sup:stop().
	
stop(_Reason) ->
	stop().

get_version() ->
	Apps = application:loaded_applications(),
	case lists:keysearch(couch, 1, Apps) of
	{value, {_, _, Vsn}} ->
		Vsn;
	false ->
		"0.0.0"
	end.
	
sup_start_link(RootDir) ->
	gen_server:start_link({local, couch_server}, couch_server, RootDir, []).

open(Filename) ->
	gen_server:call(couch_server, {open, Filename}).
	
create(Filename, Options) ->
	gen_server:call(couch_server, {create, Filename, Options}).
	
delete(Filename) ->
	gen_server:call(couch_server, {delete, Filename}).
	
init(RootDir) ->
	{ok, RegExp} = regexp:parse("^[a-z][a-z0-9\\_\\$()\\+\\-\\/]*$"),
	{ok, #server{root_dir=RootDir, dbname_regexp=RegExp}}.

check_filename(#server{dbname_regexp=RegExp}, Filename) ->
	case regexp:match(Filename, RegExp) of
	nomatch ->
		{error, illegal_database_name};
	_Match ->
		ok
	end.
	
get_full_filename(Server, Filename) ->
	filename:join([Server#server.root_dir, "./" ++ Filename ++ ".couch"]).
	

terminate(_Reason, _Server) ->	
	ok.

all_databases() ->
	{ok, Root} = gen_server:call(couch_server, get_root),
	Filenames = 
	filelib:fold_files(Root, "^[a-z0-9\\_\\$()\\+\\-]*[\\.]couch$", true,
		fun(Filename, AccIn) ->
			case Filename -- Root of
			[$/ | RelativeFilename] -> ok;
			RelativeFilename -> ok
			end,
			[filename:rootname(RelativeFilename, ".couch") | AccIn]
		end, []),
	{ok, Filenames}.
	

handle_call(get_root, _From, #server{root_dir=Root}=Server) ->
	{reply, {ok, Root}, Server};
handle_call({open, Filename}, From, Server) ->
	case check_filename(Server, Filename) of
	{error, Error} ->
		{reply, {error, Error}, Server};
	ok ->
		Filepath = get_full_filename(Server, Filename),
		Result = supervisor:start_child(couch_server_sup,
			{Filename,
				{couch_db, open, [Filename, Filepath]},
				transient ,
				infinity,
				supervisor,
				[couch_db]}),
		case Result of
		{ok, Db} ->
			{reply, {ok, Db}, Server};		
		{error, already_present} ->
			ok = supervisor:delete_child(couch_server_sup, Filename),
			% call self recursively
			handle_call({open, Filename}, From, Server);
		{error, {already_started, Db}} ->
			{reply, {ok, Db}, Server};
		{error, {not_found, _}} ->
			{reply, not_found, Server};
		{error, {Error, _}} ->
			{reply, {error, Error}, Server}
		end
	end;
handle_call({create, Filename, Options}, _From, Server) ->
	case check_filename(Server, Filename) of
	{error, Error} ->
		{reply, {error, Error}, Server};
	ok ->
		Filepath = get_full_filename(Server, Filename),
		ChildSpec = {Filename,
				{couch_db, create, [Filename, Filepath, Options]},
				transient,
				infinity,
				supervisor,
				[couch_db]},
		case supervisor:delete_child(couch_server_sup, Filename) of
		ok ->
			{reply, sup_start_child(couch_server_sup, ChildSpec), Server};			
		{error, not_found} ->
			{reply, sup_start_child(couch_server_sup, ChildSpec), Server};
		{error, running} ->
			% a server process for this database already started. Maybe kill it
			case lists:member(overwrite, Options) of
			true ->
				supervisor:terminate_child(couch_server_sup, Filename),
				ok = supervisor:delete_child(couch_server_sup, Filename),
				{reply, sup_start_child(couch_server_sup, ChildSpec), Server};
			false ->
				{reply, {error, database_already_exists}, Server}
			end
		end
	end;
handle_call({delete, Filename}, _From, Server) ->
	FullFilepath = get_full_filename(Server, Filename),
	case supervisor:terminate_child(couch_server_sup, Filename) of
	ok ->
		ok = supervisor:delete_child(couch_server_sup, Filename),
		{reply, convert_file_error(file:delete(FullFilepath)), Server};
	{error, not_found} ->
		{reply, convert_file_error(file:delete(FullFilepath)), Server};
	Error ->
		{reply, Error, Server}
	end.

convert_file_error(ok) ->
	ok;	
convert_file_error({error, enoent}) ->
	not_found;
convert_file_error(Error) ->
	Error.
	
% this function is just to strip out the child spec error stuff if hit
sup_start_child(couch_server_sup, ChildSpec) ->
	case supervisor:start_child(couch_server_sup, ChildSpec) of		
	{error, {Error, _ChildInfo}} ->
		{error, Error};
	Else ->
		Else
	end.
	
handle_cast(_Msg, State) ->
	{noreply,State}.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.
	
handle_info(_Info, State) ->
	{noreply, State}.
	