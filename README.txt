This is a readme for building the CouchDb Project.

******* Linux Build Setup *******

Your machine will need installed (examples are for debian etch):

ICU (http://icu.sourceforge.net/)
 apt-get install libicu34
 apt-get install libicu34-dev

Erlang OTP (http://erlang.org/)
 apt-get install erlang

Note: This project will build without modification with Erlang OTP R11B-1. If you do not have that version, you will get build errors and you will need to edit the package version numbers in CouchDb/couch.rel to match the ones in your Erlang install. (a future version of the build will allow any recent version of Erlang without edits)

Then execute build.sh.

It should end with the message:
 CouchDb as been built successfully!

Then become root or use sudo to install CouchDb
 ./build.sh --install=/usr/local

 The path after --install= can be anywhere you want. The installer automatically adds a couchdb directory to the path. With
 /usr/local CouchDb will installed into /usr/local/couchdb. 
 

To run CouchDb
 go to your installation directory (/usr/local/couchdb) and run ./bin/startCouchDb.sh
 if you don't want to run CouchDb as root, make sure to chown the installation directory to the user that
 executes ./bin/startCouchDb.sh.

 If an interactive Erlang shell shows up on the console and displays "CouchDb has started. Time to Relax.", relax.
 Then point your web browser to http://localhost:8888/ and look for a success message.
 Then go to the wiki (http://couchdbwiki.com/) to learn how to proceed now.


******* Windows Build Setup ********

To Build Couch, you will need:

* Microsoft Visual Studio .NET 2003

* Java JRE installed and a java executable on the path (you probably already have this).

* Install and build IBM's ICU (http://icu.sourceforge.net/).
  Set the environment variable ICU_ROOT to point the root of the ICU install, just below the "lib" and "include" directories:
	ICU_ROOT=C:\icu

* Erlang Runtime (http://erlang.org/download.html)

Note: This project will build without modification with Erlang OTP R11B-1. If you do not have that version, you will get build errors and you will need to edit the package version numbers in CouchDb/couch.rel to match the ones in your Erlang install. (a future version of the build will allow any recent version of Erlang without edits)

* set the ERLANG_ROOT environment variable to point the erlang root dir:
	ERLANG_ROOT=:\Program Files\erl5.5.1\

* set the ERTS_ROOT environment variable to point the ERTS directory:
	ERTS_ROOT=C:\Program Files\erl5.5.1\erts-5.5.1

Then execute build_win_kit.cmd to create a full couchdb install kit


******* New Version Checklist *******

1. Update version info in files: version
2. Add important information to dist/common/readme.txt

