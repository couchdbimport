// Do some basic tests.
var basic_tests = function() {
  print("Testing basic functionality");
  var db = new CouchDb("test_suite_db");
  
  // Reset the database
  db.deleteDb();
  db.createDb();

  // Get the database info, check the doc_count
  T(db.info().doc_count == 0);

  // create a document and save it to the database
  var doc = {_id:"0",a:1,b:1};
  var result = db.save(doc);
  
  T(result.ok==true); // return object has an ok member with a value true
  T(result._id); // the _id of the document is set.
  T(result._rev); // the revision id of the document is set.

  // Verify the input doc is now set with the doc id and rev 
  // (for caller convenience).
  T(doc._id == result._id && doc._rev == result._rev);

  var id = result._id; // save off the id for later

  // Create some more documents.
  // Notice the use of the ok member on the return result.
  T(db.save({_id:"1",a:2,b:4}).ok);
  T(db.save({_id:"2",a:3,b:9}).ok);
  T(db.save({_id:"3",a:4,b:16}).ok);

  // Check the database doc count
  T(db.info().doc_count == 4);

  // Check the all docs
  var results = db.all_docs();
  var docs = results.rows;

  for(var i=0; i < docs.length; i++) {
    T(docs[i]._id >= "0" && docs[i]._id <= "4");
  }

  // Test a simple map functions

  // create a map function that selects all documents whose "a" member
  // has a value of 4, and then returns the document's b value.
  var mapFunction = function(doc){
    if(doc.a==4)
      return doc.b
  };

  results = db.query(mapFunction);
  
  // verify only one document found and the result value (doc.b).
  T(results.total_rows == 1 && results.rows[0].value == 16);

  // reopen document we saved earlier
  existingDoc = db.open(id);

  T(existingDoc.a==1);

  //modify and save
  existingDoc.a=4;
  db.save(existingDoc);

  // redo the map query
  results = db.query(mapFunction);

  // the modified document should now be in the results.
  T(results.total_rows == 2);

  // write 2 more documents
  T(db.save({a:3,b:9}).ok);
  T(db.save({a:4,b:16}).ok);

  results = db.query(mapFunction);

  // 1 more document should now be in the result.
  T(results.total_rows == 3);
  T(db.info().doc_count == 6);

 // delete a document
  T(db.deleteDoc(existingDoc).ok);

  // make sure we can't open the doc
  T(db.open(existingDoc._id) == null);

  results = db.query(mapFunction);

  // 1 less document should now be in the results.
  T(results.total_rows == 2);
  T(db.info().doc_count == 5);
  print("Done");
}

// Do some edit conflict detection tests
var conflict_tests = function() {
  print("Testing conflicts");
  var db = new CouchDb("test_suite_db");
  db.deleteDb();
  db.createDb();
  
  // create a doc and save
  var doc = {_id:"foo",a:1,b:1};
  T(db.save(doc).ok);
  
  // reopen
  var doc2 = db.open(doc._id);
  
  // ensure the revisions are the same
  T(doc._id == doc2._id && doc._rev == doc2._rev);
  
  // edit the documents.
  doc.a = 2;
  doc2.a = 3;
    
  // save one document
  T(db.save(doc).ok);
  
  // save the other document
  try {
    db.save(doc2);  // this should generate a conflict exception
    T("no save conflict 1" && false); // we shouldn't hit here
  } catch (e) {}
  
  // Now clear out the _rev member and save. This indicates this document is
  // new, not based on an existing revision.
  doc2._rev = undefined;
  try {
    db.save(doc2); // this should generate a conflict exception
    T("no save conflict 2" && false); // we shouldn't hit here
  } catch (e) {}
  
  // Now delete the document from the database
  T(db.deleteDoc(doc).ok);
  
  T(db.save(doc2).ok);  // we can save a new document over a deletion without
                        // knowing the deletion rev.
  print("Done");
}

// test saving a semi-large quanitity of documents and do some view queries.
var lots_of_docs_tests = function() {
  print("Testing lots of docs"); 
  // keep number lowish for now to keep tests fasts. Crank up manually to
  // to really test.
  var numDocsToCreate = 500;
  
  var db = new CouchDb("test_suite_db");
  db.deleteDb();
  db.createDb();
  
  var docs = make_docs(numDocsToCreate);
  
  T(db.bulk_save(docs).ok);
  
  // query all documents, and return the doc.integer member as a key.
  results = db.query(function(doc){return {key:doc.integer}});
  
  T(results.total_rows == numDocsToCreate);
  
  // validate the keys are ordered ascending
  for(var i=0; i<numDocsToCreate; i++) {
    T(results.rows[i].key==i);
  }
  
  // do the query again, but reverse the output
  results = db.query(function(doc){return {key:doc.integer}}, {reverse:true});
  
  T(results.total_rows == numDocsToCreate);
   
  // validate the keys are ordered descending
  for(var i=0; i<numDocsToCreate; i++) {
    T(results.rows[numDocsToCreate-1-i].key==i);
  }
  print("Done");
}


var multiple_rows_tests = function() {
  print("Testing multiple rows"); 
  var db = new CouchDb("test_suite_db");
  db.deleteDb();
  db.createDb();
  
  var nc = {_id:"NC", cities:["Charlotte", "Raleigh"]};
  var ma = {_id:"MA", cities:["Boston", "Lowell", "Worcester", "Cambridge", "Springfield"]};
  var fl = {_id:"FL", cities:["Miami", "Tampa", "Orlando", "Springfield"]};
  
  T(db.save(nc).ok);
  T(db.save(ma).ok);
  T(db.save(fl).ok);
  
  var generateListOfCitiesAndState = function(doc){
      var rows = []
      for( var i=0; i < doc.cities.length; i++) {
        rows[i] = {key: doc.cities[i] + ", " + doc._id};
      }
      // if a function returns a object with a single "multi" member
      // then each element in the member will be output as its own row in the
      // results
      return {multi:rows}
    }

  var results = db.query(generateListOfCitiesAndState);
  var rows = results.rows;
  
  T(rows[0].key == "Boston, MA");
  T(rows[1].key == "Cambridge, MA");
  T(rows[2].key == "Charlotte, NC");
  T(rows[3].key == "Lowell, MA");
  T(rows[4].key == "Miami, FL");
  T(rows[5].key == "Orlando, FL");
  T(rows[6].key == "Raleigh, NC");
  T(rows[7].key == "Springfield, FL");
  T(rows[8].key == "Springfield, MA");
  T(rows[9].key == "Tampa, FL");
  T(rows[10].key == "Worcester, MA");
  
  // add another city to NC
  nc.cities.push("Wilmington");
  T(db.save(nc).ok);

  var results = db.query(generateListOfCitiesAndState);
  var rows = results.rows;

  T(rows[0].key == "Boston, MA");
  T(rows[1].key == "Cambridge, MA");
  T(rows[2].key == "Charlotte, NC");
  T(rows[3].key == "Lowell, MA");
  T(rows[4].key == "Miami, FL");
  T(rows[5].key == "Orlando, FL");
  T(rows[6].key == "Raleigh, NC");
  T(rows[7].key == "Springfield, FL");
  T(rows[8].key == "Springfield, MA");
  T(rows[9].key == "Tampa, FL");
  T(rows[10].key == "Wilmington, NC");
  T(rows[11].key == "Worcester, MA");
  
  // now delete MA
  T(db.deleteDoc(ma).ok);
  
  var results = db.query(generateListOfCitiesAndState);
  var rows = results.rows;
  
  T(rows[0].key == "Charlotte, NC");
  T(rows[1].key == "Miami, FL");
  T(rows[2].key == "Orlando, FL");
  T(rows[3].key == "Raleigh, NC");
  T(rows[4].key == "Springfield, FL");
  T(rows[5].key == "Tampa, FL");
  T(rows[6].key == "Wilmington, NC");
  print("Done");  
}

var large_docs = function() {
  print("Testing large documents");
  var db = new CouchDb("test_suite_db");
  db.deleteDb();
  db.createDb();
  
  var longtext = "0123456789\n";
  
  for (var i=0; i<10; i++) {
    longtext = longtext + longtext
  }
//  print(longtext);
  T(db.save({"longtest":longtext}).ok);
  T(db.save({"longtest":longtext}).ok);
  T(db.save({"longtest":longtext}).ok);
  T(db.save({"longtest":longtext}).ok);
  
  // query all documents, and return the doc.foo member as a key.
  results = db.query(function(doc){
      return doc.longtest;
    });
  
  //print(results.toSource());
  print("Done");
}


var utf8_tests = function() {
  print("Testing utf8");

  var db = new CouchDb("test_suite_db");
  db.deleteDb();
  db.createDb();
  
  var texts = [];
  
  texts[0] = "1. Ascii: hello"
  texts[1] = "2. Russian: На берегу пустынных волн"
  texts[2] = "3. Math: ∮ E⋅da = Q,  n → ∞, ∑ f(i) = ∏ g(i),"
  texts[3] = "4. Geek: STARGΛ̊TE SG-1"
  texts[4] = "5. Braille: ⡌⠁⠧⠑ ⠼⠁⠒  ⡍⠜⠇⠑⠹⠰⠎ ⡣⠕⠌"
  
  // check that we can save a reload with full fidelity
  for (var i=0; i<texts.length; i++) {
    T(db.save({_id:i.toString(), text:texts[i]}).ok);
  }
  
  for (var i=0; i<texts.length; i++) {
    T(db.open(i.toString()).text == texts[i]);
  }
  
  // check that views and key collation don't blow up
  var rows = db.query(function(doc) {return {key:doc.text}}).rows;
  
  for (var i=0; i<texts.length; i++) {
    T(rows[i].key == texts[i]);
  }
  print("Done");
}

var attachment_tests = function() {
  print("Testing attachments");
  var db = new CouchDb("test_suite_db");
  var req = new XMLHttpRequest();
  db.deleteDb();
  db.createDb();
  
  // first check json attachments
  
  var jsonData = ["this is some json data", 1, 2, ""];

  var jsonAttDoc = {
    _id:"json",
    _attachments:{
      "foo": {
        "type":"json",
        "data": jsonData
      }
    }
  }

  
  T(db.save(jsonAttDoc).ok);
  
  req.open("GET", "/test_suite_db/json?attachment=foo");
  req.send();
  
  T(req.responseText == jsonData.toJSONString())

  // now check binary attachments
  var binAttDoc = {
    _id:"bin_doc",
    _attachments:{
      "foo.txt": {
        "type":"base64",
        "data": "VGhpcyBpcyBhIGJhc2U2NCBlbmNvZGVkIHRleHQ="
      }
    }
  }

  T(db.save(binAttDoc).ok);
  
  req.open("GET", "/test_suite_db/bin_doc?attachment=foo.txt");
  req.send();
  
  T(req.responseText == "This is a base64 encoded text")
  print("Done");

}


var design_doc_tests = function() {
  print("Testing design documents");
  var db = new CouchDb("test_suite_db");
  db.deleteDb();
  db.createDb();
  
  var numDocs = 50;
  
  var designDoc = {
    _id:"_design_foo",
    language: "text/javascript",
    views: {
      all_docs: "function(doc){return {key:doc.integer}}",
      no_docs: "function(doc){}",
      single_doc: "function(doc){if(doc._id == \"1\"){return 1}}"
    }
  }
  T(db.save(designDoc).ok);
  
  T(db.bulk_save(make_docs(numDocs)).ok);
  
  rows =db.view("_design_foo:all_docs").rows
  
  for (var i=0; i < numDocs; i++) {
    T(rows[i].key == i);
  }
  
  T(db.view("_design_foo:no_docs").total_rows == 0)
  T(db.view("_design_foo:single_doc").total_rows == 1)
  print("Done");
}

var view_collation_tests = function() {
  print("Testing view collation");
  var db = new CouchDb("test_suite_db");
  db.deleteDb();
  db.createDb();
  
  // NOTE, the values are already in their correct sort order. Consider this
  // a specification of collation of json types.
  
  var values = []
  
  // special values sort befoe all other types
  values.push(undefined)  // This means the first doc doesn't have a key,
                          // causing it to sort before all other values.
  values.push(null)
  values.push(false)
  values.push(true)
  
  // then numbers
  values.push(1)
  values.push(2)
  values.push(3.0)
  values.push(4)
  
  // then text, case sensitive
  values.push("a")
  values.push("A")
  values.push("aa")
  values.push("b")
  values.push("B")
  values.push("ba")
  values.push("bb")

  // then arrays. compared element by element until different.
  // Longer arrays sort after their prefixes 
  values.push(["a"])
  values.push(["b"])
  values.push(["b","c"])
  values.push(["b","c", "a"])
  values.push(["b","d"])
  values.push(["b","d", "e"])
  
  // then object, compares each key value in the list until different.  
  // larger objects sort after their subset objects.
  values.push({a:1})
  values.push({a:2})
  values.push({b:1})
  values.push({b:2})
  values.push({b:2, a:1}) // Member order does matter for collation.
                          // CouchDb preserves member order
                          // but doesn't require that clients will.
                          // (this test might fail if used with a js engine
                          // that doesn't preserve order)
  values.push({b:2, c:2})
    
  for (var i=0; i<values.length; i++) {
    db.save({_id:(i).toString(), foo:values[i]});
  }
  
  var queryFun = function(doc) {return {key:doc.foo}}
  
  var rows = db.query(queryFun).rows
  
  for (var i=0; i<values.length; i++) {
    T(equals(rows[i].key, values[i]))
  }
  
  // everything has collated correctly. Now to check the reverse output
  
  rows = db.query(queryFun, {reverse:true}).rows
  
  for (i=0; i<values.length; i++) {
    T(equals(rows[i].key, values[values.length - 1 -i]))
  }
  
  //
  // now check the key query args
  //
  
  for (var i=1; i<values.length; i++) {
    var queryOptions =  {key:values[i]}
    var rows = db.query(queryFun, queryOptions).rows
    
    T(rows.length == 1 && equals(rows[0].key, values[i]))
  }
  
  print("Done")
}

// This is the code that runs the actual tests

run_tests(basic_tests);
run_tests(conflict_tests);
run_tests(lots_of_docs_tests);
run_tests(multiple_rows_tests);
run_tests(large_docs);
run_tests(utf8_tests);
run_tests(design_doc_tests);
run_tests(attachment_tests);
run_tests(view_collation_tests);

// If we got here, all the tests passed.
print("<h2>Tests passed!</h2>");





// *************************** Utility Functions **************************** //


// run_tests
//
// Patches and runs the function that contains the T(SomeTest) tests.
//
// It patches the function by converting test lines like this:
//  T(X==1);
// to
//  T(X==1, "X==1");
//
// When tests fail the T function will display the source code
// to make debugging the failure easier.

function run_tests(testfun) {  
  var source = testfun.toString();
  var output = "";
  var i = 0;
  var testMarker = "T("
  while (i < source.length) {
    var testStart = source.indexOf(testMarker, i);
    if (testStart == -1) {
      output = output + source.substring(i, source.length);
      break;
    }
    var testEnd = source.indexOf(");", testStart);
    var testCode = source.substring(testStart + testMarker.length, testEnd);
    output = output + source.substring(i, testStart) + "T(" + testCode + "," + testCode.toSource();
    i = testEnd;
  }

  Fun = eval(output);
  Fun();
}

// Use T to perform a test that returns false on failure and if the test fails,
// display the line that failed.
// Example:
// T(MyValue==1);
function T(arg1, arg2) {
  document.write(". ");
  if (!arg1) {
      document.writeln("<p><b>Error, failed test:</b><br>T(" + arg2 + ");</p>");
      throw "Error, failed test: T(" + arg2 + ")";
  }
}

// Prints messages to the webpage.
function print(msg) {
  document.write("<p>" + msg + "</p>");
}

function make_docs(n, templateDoc) {
  var templateDocSrc = templateDoc ? templateDoc.toSource() : "{}"
  var docs = []
  for(var i=0; i<n; i++) {
    var newDoc = eval("(" + templateDocSrc + ")");
    newDoc._id = (i).toString();
    newDoc.integer = i
    newDoc.string = (i).toString();
    docs.push(newDoc)
  }
  return docs;
}

function equals(a,b) {
  if (a === b) return true;
  try {
    return a.toSource() === b.toSource()
  } catch (e) {
    return false;
  }
}
