
// A simple class to represent a database. Uses XMLHttpRequest
// to interface with the CouchDb server.

function CouchDb(dbname) {
  this.dbUrl = "/" + dbname +"/";
  this.req = new XMLHttpRequest();
  
  // Creates the database on the server
  this.createDb = function() {
    this.req.open("PUT", this.dbUrl, false);
    this.req.send(null);
    if (this.req.status != 201)
      throw this.req.responseText.parseJSON();
    return this.req.responseText.parseJSON();
  }
  
  // Deletes the database on the server
  this.deleteDb = function() {
    this.req.open("DELETE", this.dbUrl, false);
    this.req.send(null);
    if (this.req.status == 404)
      return false;
    if (this.req.status != 202)
      throw this.req.responseText.parseJSON();
    return this.req.responseText.parseJSON();
  }

  // Save a document to the database
  this.save = function(doc, options) {
    if (doc._id == undefined) 
      this.req.open("POST", this.dbUrl + this.options_to_str(options), false);
    else
      this.req.open("PUT", this.dbUrl  + doc._id + this.options_to_str(options), false);
    this.req.send(doc.toJSONString());
    if (this.req.status != 201)
      throw this.req.responseText.parseJSON();
    var result = this.req.responseText.parseJSON();
    // set the _id and _rev members on the input object, for caller convenience.
    doc._id = result._id;
    doc._rev = result._rev;
    return result;
  }
  
  // Open a document from the database
  this.open = function(docid, options) {
    this.req.open("GET", this.dbUrl + docid + this.options_to_str(options), false);
    this.req.send(null);
    if (this.req.status == 404)
      return null;
    if (this.req.status != 200)
      throw this.req.responseText.parseJSON();
    return this.req.responseText.parseJSON();
  }
  
  // Deletes a document from the database.
  this.deleteDoc = function(doc) {
    this.req.open("DELETE", this.dbUrl + doc._id + "?rev=" + doc._rev, false);
    this.req.send(null);
    if (this.req.status != 202)
      throw this.req.responseText.parseJSON();
    var result = this.req.responseText.parseJSON();
    doc._rev = result._rev; //record rev in input document
    doc._deleted = true;
    return result;
  }
  
  this.bulk_save = function(docs, options) {
      this.req.open("POST", this.dbUrl + this.options_to_str(options), false);
      this.req.send({_bulk_docs:docs}.toJSONString());
      if (this.req.status != 201)
        throw this.req.responseText.parseJSON();
      return this.req.responseText.parseJSON();
  } 
  
  // Applies the map function to the contents of database and returns the results.
  this.query = function(mapFun, options) {
    this.req.open("POST", this.dbUrl + "_temp_view" + this.options_to_str(options), false);
    this.req.send(mapFun.toSource());
    if (this.req.status != 200)
      throw this.req.responseText.parseJSON();
    return this.req.responseText.parseJSON(); 
  }
  
  this.view = function(viewname, options) {
    return this.open(viewname, options);
  }
  
  // gets information about the database
  this.info = function() {
    this.req.open("GET", this.dbUrl, false);
    this.req.send("");
    if (this.req.status != 200)
      throw this.req.responseText.parseJSON();
    return this.req.responseText.parseJSON();
  }
  
  this.all_docs = function(options) {
    this.req.open("GET", this.dbUrl + "_all_docs" + this.options_to_str(options), false);
    this.req.send(null);
    if (this.req.status != 200)
      throw this.req.responseText.parseJSON();
    return this.req.responseText.parseJSON();
  }
  
  // Convert a options object to an url query string.
  // ex: {key:'value',key2:'value2'} becomes '?key="value"&key2="value2"'
  this.options_to_str = function(optionsobj) {
    var query_str = ""
    if (typeof optionsobj == "object") {
      for(var option in optionsobj) {
        if (typeof optionsobj[option] != "function") { //skip any methods
          if (query_str == "")
            query_str =  "?" + option + "=" + this._json(optionsobj[option])
          else
            query_str = query_str + "&" + option + "=" + this._json(optionsobj[option])
        }
      }
    }
    return query_str;
  }
  
  this._json = function(obj) {
    if (obj === null)
      return "null"
    else
      return obj.toJSONString()
  }
}


