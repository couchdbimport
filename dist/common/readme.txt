
To start CouchDb server, run ./bin/startCouchDb.sh

Go to http://couchdb.com/ for more information and documentation.

-- What's new in Version 0.6.4 --

Fabric is no longer. Neither is XML. CouchDb now uses JSON and Javascript
for data transport and View definitions.

-- What's new in Version 0.6.0 --


# Replication

The replication facility is now available in CouchDb. To replicate a database, from the erlang console:

> couch_rep:replicate("local_database_name_a", "local_database_name_b").

You can also specify a HTTP URL to a remote database:

> couch_rep:replicate("local_database_name_a","http://remoteserver:8888/remote_database_name_b/").

Either or both databases can be remote:

> couch_rep:replicate("http://remoteserver:8888/remote_database_name_a/", "http://remoteserver:8888/remote_database_name_b/").



# CouchPeek

CouchPeek can now create and delete documents and view their XML source, as well as display tables.



# Build from Source

The process of building from source is made easier and less error prone.


# CouchPeek

CouchPeek can now create and delete documents and view their XML source, as well as display tables.



# Build from Source

The process of building from source is made easier and less error prone.




-- What's new in Version 0.5.0 --

# Built-in CouchPeek Utility.

Visit special URL at couchDb server:
http://couchserver.net:8888/$utils/peek.html


# Full install kit buildable from a single command.

# New Linux version. Mac version coming soon


-- What's new in Version 0.4.0 --

# Error logging

Couch error codes and messages are no longer sent in the HTTP fields, instead they are exclusively returned in the XML body. This is to avoid HTTP header parsing problems with oddly formed error messages.

Returned error messages are now logged at the server at the "info" level, to make general debugging easier.


# Fabric/Table building timeouts

Fixed a problem where big table builds could cause timesout errors.


# Fabric

Lots of changes in the low level machinary but most formulas will continue to function the same.

MOST IMPORTANT CHANGE:

New nil lists behaviour. Non-existant variables are nil lists. 

Previously, to test for a balnk field, you'd do this:

x == ""

The preceeding NOW RETURNS FALSE if X is not found. Instead, this is necessary:

x == nil

For complete behaviours, download the source and search for "nil" in the Fabric tests.

Added full compiler support for extended characters in formula source.

Support for Perl/Ruby like regular expressions:
Foo =~ /regex/
Foo =~ /regex/i

# Compute Tables 

Added two new attributes to tables, total_rows and result_start.

Total_rows is an integer of the total number of rows in the table. result_start is the row offest of the first row returned. "1" means the first row returned is the first row in the table, "6" means the first row returned is actually the 6th row in the table.




-- What's new in Version 0.3.0 --

# UTF8

CouchDb now fully supports UTF8 and locale specific collation via the ICU library, both in the fabric engine and in collation used in computed tables.


# Fabric

New Fabric operator "in". Example:
 SELECT "Bob" in NamesList

uppercase
lowercase
left
right
leftback
rightback
split/explode
join/implode
length
count
list_trim
trim
full_trim
matches
iserror
number
docfields
getfield


# Table get URL arguments

New url arguments available:

startdoc=docid
skip=N

startdoc specifies the starting document to use if there are multiple rows with identical startkeys.

NOTE!!! startdoc can ONLY be used in conjunction with startkey(s) only. It doesn't work on its own!

skip=N specifies the number of rows to skip before returning results. N must be a positive integer. if used with a "count=M" parameter, the skipped rows aren't counted as output and M rows will be returned.



# XML changes

<doc>
 <field id="foo">
  <text>bar</text>
 </field>
</doc>

becomes:

<doc>
 <field name="foo">
  <text>bar</text>
 </field>
</doc>


--

<table ...>
 <tr id="somedoc">
  <td id="columnName>
   <text>some text</text>
  </td>
 </tr>
</table>


becomes:

<table ..>
 <row docid="somedoc" rev="4541A2">
  <column name="columnName>
   <text>some text</text>
  </td>
 </tr>
</table>

--

When putting a document,

<doc ... previous_rev="...">

becomes:

<doc ... rev="...">


--

$all_docs 


<table id="$all_docs" ...>
 <tr id="doc1">
   <td id="docid">adoc</td>
   <td id="rev">2773825291</td>
  </tr>
  <tr id="doc2">
   <td id="docid">anotherdoc</td>
   <td id="rev">380240121</td>
  </tr>
</table>

becomes:


<table id="$all_docs" ...>
 <row docid="doc1" rev="4541A2" />
 <row docid="doc2" rev="41A254" />
</table>

