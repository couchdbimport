@REM if couch.ini doesn't exist, it's setup time

@IF NOT EXIST couch.ini copy couch_httpd.conf.src couch_httpd.conf
@IF NOT EXIST couch.ini copy couch.ini.src couch.ini

start erl.exe -sasl errlog_type error -boot couch