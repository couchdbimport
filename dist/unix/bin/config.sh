#!/bin/sh
#
# CouchDb config.sh 
# (c) 2006 Jan Lehnardt <jan@php.net>
# This is free software. See license.txt

CONFIG=dist/unix/couch.ini
CONFIG_SRC=dist/unix/couch.ini.src

# call with couch_config_replace_value "FABRICSERVER" "FabricServer"
couch_config_replace_value()
{
	sed \
	-e "s#@$1@#$2#" \
	< $CONFIG > $CONFIG.new
	
	mv $CONFIG.new $CONFIG
}

# Dont do anything when there is a $CONFIG file
if [ -f $CONFIG ]; then
	echo "CouchDb is already configured."
	exit 0;
fi


cp $CONFIG_SRC $CONFIG

couch_config_replace_value "COUCH_ERL_DRIVER_DIR" "lib"


# get erts/bin dir
ERLANG_ROOT_DIR=`grep ROOTDIR= $ERL|sed -e 's/ROOTDIR=//'`
ERTS_BIN_DIR=`grep BINDIR= $ERL|sed -e 's/BINDIR=\$ROOTDIR\///'`

ERTS_BIN_DIR=$ERLANG_ROOT_DIR/$ERTS_BIN_DIR

CONFIG=dist/unix/bin/couch_erl
CONFIG_SRC=dist/unix/bin/couch_erl.src

cp $CONFIG_SRC $CONFIG

couch_config_replace_value "COUCH_INSTALL_DIR" "$COUCHDB_INSTALL_DIR"
couch_config_replace_value "ERTS_BIN_DIR" "$ERTS_BIN_DIR"
# Done