# CouchDb Makefile
# (c) 2006 Jan Lehnardt <jan@php.net>
# This program is Free Software. See license.txt


UNICODE_INCLUDE_DIR ?= $(shell icu-config --prefix)
UNICODE_LIB_DIR ?= $(shell icu-config --icudata-install-dir)
ERLANG_INCLUDE_DIR ?= /usr/lib/erlang/driver
COUCHDB_INSTALL_DIR ?= /usr/local/couchdb
ERL ?= /usr/bin/erl
ERLC ?= /usr/bin/erlc

BUILD_DIR ?= ./build
DIST_DIR ?= ./dist

GCC ?= /usr/bin/gcc
CPP ?= /usr/bin/g++
INSTALL ?= /usr/bin/install

CPPFLAGS=-D_DEBUG
LDFLAGS=-L$(UNICODE_LIB_DIR) -L$(BUILD_DIR)/

install: couchdb
	cp -R dist/common/* $(COUCHDB_INSTALL_DIR)/
	mkdir $(COUCHDB_INSTALL_DIR)/lib $(COUCHDB_INSTALL_DIR)/bin $(COUCHDB_INSTALL_DIR)/boot $(COUCHDB_INSTALL_DIR)/js
	cp src/js/js src/js/json.js src/js/main.js $(COUCHDB_INSTALL_DIR)/js/
	cp dist/unix/bin/startCouchDb.sh $(COUCHDB_INSTALL_DIR)/bin/
	cp $(BUILD_DIR)/couch_erl_driver.so $(COUCHDB_INSTALL_DIR)/lib/
	mv $(COUCHDB_INSTALL_DIR)/couch_httpd.conf.src $(COUCHDB_INSTALL_DIR)/couch_httpd.conf
	./dist/unix/bin/config.sh
	mv ./dist/unix/couch.ini $(COUCHDB_INSTALL_DIR)/
	mv ./dist/unix/bin/couch_erl $(COUCHDB_INSTALL_DIR)/bin/couch_erl
	chmod +x $(COUCHDB_INSTALL_DIR)/bin/couch_erl

	cp couch.boot $(COUCHDB_INSTALL_DIR)/boot/

	$(ERL) -noshell -run  build_couch output_full_libs $(COUCHDB_INSTALL_DIR)

#couchdb

couchdb: host $(BUILD_DIR)/couch_erl_driver.so build/build_couch.beam build/couch.boot
	cd src/js && make -f Makefile.ref && cd ../../
#	$(ERL) -noshell -run build/build_couch compile_all

	@echo "CouchDb has been built successfully!"

host:
ifeq ($(strip $(HOST)),)
	@echo "Cannot determine HOST. exiting...";
	exit 1;
endif
	

$(BUILD_DIR)/couch_erl_driver.so:
ifeq ($(HOST),Linux)
	$(GCC) -w -rdynamic -shared -fPIC -I$(UNICODE_INCLUDE_DIR) -I$(ERLANG_INCLUDE_DIR) src/CouchDb/couch_erl_driver.c $(LDFLAGS) -o $(BUILD_DIR)/couch_erl_driver.so -licuuc -licudata -licui18n
endif

ifeq ($(HOST),Darwin)
	$(GCC) -w -bundle -flat_namespace -undefined suppress -I$(UNICODE_INCLUDE_DIR) -I$(ERLANG_INCLUDE_DIR) src/CouchDb/couch_erl_driver.c $(LDFLAGS) -o $(BUILD_DIR)/couch_erl_driver.so -licudata -licuuc -licui18n
endif
	

build/couch.boot:
	$(ERL) -noshell -run build_couch compile_all

build/build_couch.beam: build/build_couch.erl
	$(ERLC) build/build_couch.erl

# clean
clean: clean_all clean_js
	
clean_js:
	@cd src/js; make -f Makefile.ref clobber
	
clean_all: 
	rm -rf src/CouchDb/*.beam src/couch_inets/*.beam build/couch.rel build/couch.app build/couch_erl_driver.so build_couch.beam couch.script couch.boot

.PHONY: clean CouchDb host dist clean_js
